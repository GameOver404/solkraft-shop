# from django.shortcuts import render
# from .forms import ContactForm

from django.conf import settings
from django.core.mail import send_mail
from django.shortcuts import render
from .forms import ContactForm


def contact_view(request):
    # If the user posts the form,
    if request.method == 'POST':
        form = ContactForm(request.POST)
        # we need to check whether the form is valid and save it to the database if it is
        if form.is_valid():
            form.save()
            # Moreover, the email is sent to Solkraft@gmail.com to let our staff know that a new inquiry was made
            email_subject = f'New contact {form.cleaned_data["email"]}: {form.cleaned_data["subject"]}'
            email_message = form.cleaned_data['message']
            # Our mails are sent with the help of SendGrid, as we've found that Google doesn't work properly with Django in our case
            send_mail(email_subject, email_message,
                      settings.CONTACT_EMAIL, settings.ADMIN_EMAIL)
            # Once the mail is sent successfully, a success page is displayed to the user
            return render(request, 'contact/success.html')
    # Of course the ContactForm is shown to the user initially - before they potentially post it
    form = ContactForm()
    return render(request, 'contact/contact.html', {'form': form})

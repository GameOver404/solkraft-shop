from django.contrib import admin
from .models import Contact

# The contact (message) can be viewed and deleted in the admin panel
admin.site.register(Contact)

from django.contrib import admin
from django.urls import path
from contact import views as contact_views

# The name is used to identify the app
# If one wants to use any function, in this case contact, one needs to write contact:contact, instead of simply contact
# One benefit of that is that one directly knows where the view function is located, which improves maintainability
# Another benefit is that contact can be used as a name for another path in another application
app_name = 'contact'

urlpatterns = [
    #path('admin/', admin.site.urls),
    path('contact/', contact_views.contact_view, name='contact'),
]

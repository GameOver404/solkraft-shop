from django.forms import ModelForm
from .models import Contact


class ContactForm(ModelForm):
    class Meta:
        # Our ContactForm uses all of the fields from the Contact model
        model = Contact
        fields = '__all__'

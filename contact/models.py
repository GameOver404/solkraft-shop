from django.db import models


class Contact(models.Model):
    email = models.EmailField()
    # written in German, since it is shown that way in the webshop
    subject = models.CharField(max_length=255)
    # and it would look weird if it is displayed in English in a German webshop
    message = models.TextField()

    def __str__(self):
        return self.email

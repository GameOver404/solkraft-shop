from django.urls import path
from .views import questions, askquestion, ajaxanswerquestion, viewquestion

urlpatterns = [
    # all questions with responses
    path('', questions, name='questions'),
    # view single question
    path('question/<int:qid>/<slug:qslug>', viewquestion, name='viewquestion'),
    # ask a question yourself
    path('ask-question', askquestion, name='askquestion'),
    # post and save answer (also in DB)
    path('ajax-answer-question', ajaxanswerquestion, name='ajaxanswerquestion')
]

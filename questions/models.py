from django.db import models
from django.utils.text import slugify

# For a Q&A a question model is needed


class Question(models.Model):
    # It has a primary key, which is used to distinguish it from other questions
    qid = models.AutoField(primary_key=True)
    # A title to easily understand what the question is about
    question_title = models.CharField(max_length=100)
    # A text which describes the question further
    question_text = models.TextField(max_length=50000)
    # The date on which the question was posted (e.g. for ordering)
    date_posted = models.DateTimeField(auto_now_add=True)
    # Name of the poster (poster does not need to be logged in)
    posted_by = models.TextField(max_length=20)
    # Slug is needed to "speak" with the model (instance)
    slug = models.SlugField(max_length=40)

    # sort by date posted, latest question first
    class Meta:
        ordering = ['-date_posted', ]

    # The question needs to be saved
    def save(self, *args, **kwargs):
        # The slug is used on the question title
        self.slug = slugify(self.question_title)
        # and the question is saved to the DB
        super(Question, self).save(*args, **kwargs)

# of course, answers are also needed in an Q&A


class Answer(models.Model):
    # It also has a primary key
    aid = models.AutoField(primary_key=True)
    # And the question id is used to link the answers to the questions
    qid = models.ForeignKey(Question, on_delete=models.CASCADE)
    # The answer needs to have a text (the actual answer)
    answer_text = models.TextField(max_length=50000)
    # We also show the date of the posting (e.g. for odering)
    date_posted = models.DateTimeField(auto_now_add=True)
    # and the name of the poster (who does not need to have an account either)
    posted_by = models.TextField(max_length=20)

    # sort by date posted, newest answer last
    class Meta:
        ordering = ['date_posted', ]

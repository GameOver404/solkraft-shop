"""Solkraft URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static
from shop import views

# These are the root url patterns

urlpatterns = [
    # The admin panel is included
    path('admin/', admin.site.urls),
    # As well as all the urls from the different applications that we've created
    path('questions/', include('questions.urls')),
    path('plate/', include('django_spaghetti.urls')),
    path('coupons/', include('coupons.urls', namespace='coupons')),
    path('blog/', include('blog.urls')),
    path('contact/', include('contact.urls')),
    path('', include('shop.urls', namespace='shop')),
    path('cart/', include('cart.urls', namespace='cart')),
    path('account/', include('account.urls')),
    path('profile/', include('customer.urls', namespace='profile')),
    path('staff/', include('staff.urls', namespace='staff')),
    path('parameter-autocomplete/', views.ParameterAutocomplete,
         name='parameter-autocomplete'),
    # and social-auth from Django social auth to add registration and login via social media
    # The login with Facebook and Google worked fine, but solely with an older version of django-social-auth which sometimes led to installation problems and thus we removed it from our website
    # path('social-auth/', include('social_django.urls', namespace='social')),
    # and two more paths from our side
    path('newsletter/', include('newsletter.urls')),
    path("password_reset", views.password_reset_request, name="password_reset"),
]

# Setting the media folder
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)

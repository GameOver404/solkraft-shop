from django import forms


class CouponApplyForm(forms.Form):
    # One can enter the coupon code in the cart
    code = forms.CharField(label='Code')

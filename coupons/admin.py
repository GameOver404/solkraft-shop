from django.contrib import admin
from .models import Coupon


class CouponAdmin(admin.ModelAdmin):
    # Our staff can view the coupons, filter for attributes of coupons or use the search to find a specific coupon
    list_display = ['code', 'valid_from', 'valid_to', 'discount', 'active']
    list_filter = ['active', 'valid_from', 'valid_to']
    search_fields = ['code']


# The admin is able to create, update and delete coupons
admin.site.register(Coupon, CouponAdmin)

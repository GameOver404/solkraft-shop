from django.urls import path
from . import views

# The app_name is used to identify the application
# If one wants to use any function; In this case apply, one needs to write coupons:apply, instead of simply apply
# One benefit of that is that one directly knows where the view function is located, which improves maintainability
app_name = 'coupons'
urlpatterns = [
    path('apply/', views.coupon_apply, name='apply'),
]

from django.shortcuts import render, redirect
from django.utils import timezone
from django.views.decorators.http import require_POST
from .models import Coupon
from .forms import CouponApplyForm


@require_POST
def coupon_apply(request):
    # We need the current CET time to check whether the item is valid, as our customer could also buy whilst being on vacation/in a different timezone
    now = timezone.now()
    form = CouponApplyForm(request.POST)
    # If the form is valid, we will try to get the Coupon object to give the customer the reduction
    if form.is_valid():
        code = form.cleaned_data['code']
    try:
        coupon = Coupon.objects.get(
            code__iexact=code, valid_from__lte=now, valid_to__gte=now, active=True)
        request.session['coupon_id'] = coupon.id
        # If the coupon does not exist, then we of course cannot give any discount to the user
    except Coupon.DoesNotExist:
        request.session['coupon_id'] = None
    return redirect('cart:cart_detail')

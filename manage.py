#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys


def main():
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Solkraft.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Django konnte nicht importiert werden. Sind Sie sicher, dass Django installiert wurde "
            "und in Ihrem PYTHONPATH als Umgebungsvariable eingetragen ist? Haben Sie eventuell "
            "vergessen eine virtuelle Umgebung zu aktivieren?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()

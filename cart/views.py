from coupons.forms import CouponApplyForm
from django.core.mail import EmailMessage
from django.contrib.auth.decorators import login_required
# WeasyPrint and BytesIO are used for creating PDF files for the orders
import weasyprint
from io import BytesIO
from django.utils import timezone
import datetime
from django.shortcuts import render, redirect, get_object_or_404, reverse, HttpResponseRedirect
from django.template.loader import render_to_string
from cart.models import DeliveryType
from cart.delivery import Delivery
from django.views.decorators.http import require_POST
from shop.models import Product
from decimal import Decimal
from .cart import Cart
from .forms import CartAddProductForm, ChooseDeliveryType, AddressForm, CartRemoveProductForm
from cart.models import Order, OrderComponent
from django.http import Http404
from staff.views import send_pdf
from cart.paypal_payment import *

# If one buys products for 65€ or more, the shipping is paid by Solkraft
FREE_DELIVERY_PRICE = 65


@require_POST
# This method requires a HTTP Post request
def cart_add_one(request, product_id):
    # The customer can add a product to the cart
    cart = Cart(request)
    # Thus, we need to get that specific product
    product = get_object_or_404(Product, id=product_id)
    # add the product to the cart
    cart.add(product)
    # and redirect the user to the cart
    return redirect('cart:cart_detail')


@require_POST
# This method requires a HTTP Post request
def cart_add(request, product_id):
    # As we also want to offer the option that the customer can choose the quantity when he/she is in the product detail page
    cart = Cart(request)
    # We need to get the product as well
    product = get_object_or_404(Product, id=product_id)
    # But this time we need to use the CartAddProductForm
    form = CartAddProductForm(request.POST)
    # If that form is valid,
    if form.is_valid():
        # we clean the data and use the add function to add the product with the right quantity to the cart
        cd = form.cleaned_data
        cart.add(product=product,
                 quantity=cd['quantity'])
    # The user gets redirected to his/her cart
    return redirect('cart:cart_detail')


@require_POST
# This method requires a HTTP Post request
def cart_remove_one(request, product_id):
    # The customer can reduce the quantity of one product by one
    cart = Cart(request)
    # Hence, we need to get the right product
    product = get_object_or_404(Product, id=product_id)
    # and the users HTTP Post request
    form = CartRemoveProductForm(request.POST)
    # If the form is valid,
    if form.is_valid():
        # We clean the data
        cd = form.cleaned_data
        # and remove the product from the cart or reduce its quantity by one
        cart.remove_one(product=product,
                        quantity=1)
    # Afterwards the customer gets redirected to his/her cart with the updated data
    return redirect('cart:cart_detail')


def cart_remove(request, product_id):
    # The user can also remove a product from his/her cart
    cart = Cart(request)
    # Thus, we need to get the product the user wants to delete
    product = get_object_or_404(Product, id=product_id)
    # and remove it from the cart
    cart.remove(product)
    # and then we redirect the customer to his/her cart
    return redirect('cart:cart_detail')


def cart_detail(request):
    # The user needs to view his/her cart
    cart = Cart(request)
    # Initially no delivery form is chosen, as the customer is free to choose one his-/herself
    delivery_form = None
    # If the user is authenticated,
    if request.user.is_authenticated:
        # he/she can choose the delivery type
        delivery_form = ChooseDeliveryType(request.POST)
        # If the user chose a delivery option and the form is valid
        if delivery_form.is_valid():
            # We copy the choice
            choice = request.POST.copy()
            # to parse the delivery type into an integer
            choice = int(choice.get('delivery_type'))
            # We also initiate the Delivery with the total price and the free delivery price (65€)
            delivery = Delivery(cart.get_total_price(), FREE_DELIVERY_PRICE)
            # We set the delivery choice from the customer
            delivery.set_delivery(choice)
            # If a discount exists, we need it as well
            discount = cart.get_discount()
            # to create a new order
            new_order = Order(user=request.user, is_confirmed=False, delivery_price=delivery.get_delivery_price(
            ), delivery_type=delivery.delivery_name, expiration_date=timezone.now() + datetime.timedelta(days=7),
                discount=discount)
            # The new order is then saved to the database
            new_order.save()
            # and the order id gets set to the new order id
            order_id = new_order.id
            # The customer is then redirected to a page where he/she can fill in the shipping address
            return redirect('cart:choose_address', order_id)
    # If there was no POST request yet, we need to show the CouponApplyForm to the customer
    coupon_apply_form = CouponApplyForm()
    # We define the context for the dynamic content of our website
    context = {'cart': cart,
               'free_delivery_price': FREE_DELIVERY_PRICE,
               'delivery_form': delivery_form,
               'coupon_apply_form': coupon_apply_form}
    # and return the cart with the needed context
    return render(request, 'cart/checkout.html', context)


@login_required(login_url='account:login')
# The user needs to be logged in to choose his/her address. Considering that the user could get back to the page on accident, we've set a redirect url for users who are not logged in
def choose_address(request, id):
    # The AddressForm needs to be filled in
    address_form = AddressForm(request.POST)
    # We need to link the address to the specific order
    order = get_object_or_404(Order, id=id)
    # If the request user is not the user who placed the order, we raise a 404 error
    if request.user != order.user:
        raise Http404
    # Otherwise we can check, if there is a post request
    else:
        # and whether the form is valid, if there was a post request
        if request.method == "POST" and address_form.is_valid():
            # We get the data from the address form
            address_form = address_form.cleaned_data
            # and we put all of the relevant information into the order fields
            order.name = str(address_form['name'])
            order.surname = str(address_form['surname'])
            # All of these fields exist within the Order class and thus we can simply fill them with the information
            order.address = str(
                address_form['street']) + '/' + str(address_form['number'])
            # Initially all that data is set to None
            order.city = str(address_form['city'])
            # and the None now gets replaced by the actual data
            order.postal_code = str(address_form['postal_code'])
            # We need to get the current cart
            cart = Cart(request)
            # and loop through every item in the cart
            for item in cart:
                # to get the product
                product = item['product']
                # and create the OrderComponent with the needed information. The order component is the product which needs to be packaged.
                comp = OrderComponent(order=order, product=product, price=product.return_price(
                ), quantity=item['quantity'])
                # We need to save the OrderComponent to the database
                comp.save()
            # and we also need to save the order to the database
            order.save()
            # Afterwards we delete the session cookies. Thus, deleting every item from the cart.
            cart.clear()
            # The current coupon id is set to None, as the voucher was used and a new session started
            request.session['coupon_id'] = None
            # We redirect the customer to the payment.html
            return redirect('cart:cart_checkout', id)
    # We define the context
    context = {'address_form': address_form, 'order': order}
    # We return the address.html with the relevant context data
    return render(request, 'cart/address.html', context)


@login_required
# The user needs to be logged in to get to that page
def cart_checkout(request, id):
    # The specific order is needed for the checkout
    order = get_object_or_404(Order, id=id)
    # We get all the order components, i.e. products, from the order
    components = OrderComponent.objects.filter(order=order)
    # If a HTTP post request exists,
    if request.method == 'POST':
        # The buy_now method below is carried out
        return buy_now(request, id)
    # The order and its components are needed as context
    context = {'order': order, 'components': components}
    # and then the page is shown to the user
    return render(request, 'cart/payment.html', context)


@login_required
# The user must be logged in to get to that page
def order_complete(request):
    # The order is initialized with None
    order = None
    # A message is created, if the transaction is successfully completed
    message = 'Die Transaktion wurde erfolgreich abgeschlossen.'
    # If the payment wasn't executed yet,
    if execute_payment(request) == False:
        # a message is shown to the customer that he/she still has to wait
        message = 'Warten auf Abschluss der Transaktion...'
    # Otherwise the payment was executed sucessfully.
    else:
        # We need to get the correct order
        order = get_object_or_404(Order, user=request.user, payment_id=str(
            request.GET.get('paymentId', None)))
        # and set is_confirmed to true, because the customer has paid his/her order
        order.is_confirmed = True
        # We save the change to the database
        order.save()
        # and send the order confirmation/invoice via e-mail as an attached PDF-file to the customer.
        send_pdf(order)
    # The message and the order are needed as context.
    context = {'message': message, 'order': order}
    # The page is returnefd along with the context.
    return render(request, 'cart/order_complete.html', context)


@login_required
def order_bank_transfer(request):
    # some information for the customer
    message = "Bitte zahlen Sie Ihre Bestellung innerhalb von 3-4 Tagen."
    # We need to get the correct order
    order = Order.objects.filter(user=request.user).last()
    # If the customer chose collection, but decides to pay with bank transfer, we'll have to change the delivery type.
    if order.delivery_type == 'Abholung':
        order.delivery_type = 'Paketversand'
        order.delivery_price = Decimal(4.95).quantize(Decimal('0.01'))
    # We set the order payment id to Banküberweisung, as the customer will pay via bank remittance
    order.payment_id = 'Überweisung'
    # Of course, we need to save the change to the database
    order.save()
    # invoice must be sent
    send_pdf(order)
    # Create the context for the dynamic page
    context = context = {'message': message, 'order': order}
    # Render the page for the customer
    return render(request, 'cart/order_complete.html', context)


@login_required
def cash_on_delivery(request):
    # information for the customer
    message = "Ihr Paket wird nun sorgfältig von uns verpackt und an die von Ihnen angegebene Lieferadresse versendet. Halten Sie als Neukunde bitte das Geld für die Rechnung inkl. Nachnahmegebühr bereit."
    # We need to get the correct order
    order = Order.objects.filter(user=request.user).last()
    # We set the order payment id to Rechnung, as the customer buys on account (unless he/she hasn't paid an order from Solkraft yet)
    order.payment_id = 'Rechnung'
    # If the customer chose collection, but then decided to pay via cash on delivery, we need to change the delivery type and price
    if order.delivery_type == 'Abholung':
        order.delivery_type = 'Paketversand'
        order.delivery_price = Decimal(4.95).quantize(Decimal('0.01'))
        # If the customer hasn't paid any orders from Solkraft yet, he/she needs to pay via cash on delivery
        order.payment_id = 'Nachnahme'

    # We need to let our staff know that the parcel needs to be packaged, even though it isn't paid yet.
    order.is_cash_on_delivery = True
    # Save the changes to the database
    order.save()
    # We also need to get all of the orders from that specific user, which were already paid.
    orders = Order.objects.filter(user=request.user, is_confirmed=True)
    # If the user hasn't paid any of his/her orders yet, then Solkraft adds the cash on delivery fee to the invoice to make sure that the customer has the money ready
    if orders.count() == 0:
        order.delivery_price = order.delivery_price + \
            Decimal(6.90).quantize(Decimal('0.01'))
        # We need to save the changes in our database
        order.save()
    # invoice must be sent
    send_pdf(order)
    # Create the context for the dynamic page
    context = context = {'message': message, 'order': order}
    # Render the page for the customer
    return render(request, 'cart/order_complete.html', context)


@ login_required
def collection(request):
    # information for the customer
    message = "Please pick up your order within the next 7 days at Solkraft in 67547 Worms, In den Gärten 72. You will find the pick-up times in the invoice sent to your deposited e-mail address.."
    # We need to get the correct order
    order = Order.objects.filter(user=request.user).last()
    # If the customer chose any other delivery type, we need to change it.
    if order.delivery_price > 0.00:
        order.delivery_type = 'Collection'
        order.delivery_price = Decimal(0.00).quantize(Decimal('0.01'))
    # We set the order payment id to 4 zeros, as this is a attribute which is solely needed for PayPal
    order.payment_id = 'Collection'
    # Of course, we need to save the change to the database
    order.save()
    # invoice must be sent
    send_pdf(order)
    # Create the context for the dynamic page
    context = context = {'message': message, 'order': order}
    # Render the page for the customer
    return render(request, 'cart/order_complete.html', context)


@ login_required
# The user must be logged in to pay his/her order
def buy_now(request, id):
    # We need to get the right order
    order = get_object_or_404(Order, id=id)
    # If the user is not equal to the one who has placed the order,
    if (order.user != request.user):
        # we raise a 404 exception
        raise Http404

    # The customer will receive the parcel from the shipment company, if he/she chooses anything but Abholung (collection)
    if order.delivery_type == 'Collection':
        order.delivery_type = 'Shipment'
        order.delivery_price = Decimal(4.95).quantize(Decimal('0.01'))
        order.save()

    # We need to create/define the payment
    payment = PaypalPayment(request, order.total_price)
    # to carry it out
    payment_details = payment.make_payment()
    # One needs to define the redirection data, which is returned later
    redirection, payment_id = payment.authorize_payment()
    # The Order model contains the attribute payment_id which is set to the current payment id
    order.payment_id = payment_id
    # We also need to tell our staff that the order was paid
    order.is_confirmed = True
    # The changes are saved to the database
    order.save()
    # The user is then redirected to authorize the payment
    return redirect(redirection)

from django.utils.safestring import mark_safe
from django.contrib import admin
# PDF which is created after the order is placed
from staff.admin import order_pdf
from django.urls import reverse
from .models import DeliveryType, Order, OrderComponent


@admin.register(Order)
# We need to register the orders, since our staff members need to be able to work with them
class OrderAdmin(admin.ModelAdmin):
    # For one, they can filter the orders by is sent and is confirmed
    list_filter = ['is_sent', 'is_confirmed']
    # An order is confirmed, once it was paid by the customer
    # The order pdf can be viewed by our staff members throught the admin panel as well
    list_display = ['__str__', 'is_confirmed', 'is_sent', order_pdf]
    pass


@admin.register(DeliveryType)
# We need to register the delivery type, so that our employees can add/delete and update them
class DeliveryTypeAdmin(admin.ModelAdmin):
    # Additional functionality is not needed
    pass


@admin.register(OrderComponent)
# The order component is all about the products which belong to that specific order
class OrderComponentAdmin(admin.ModelAdmin):
    # What is especially important is, that the employee can easily see whether the product was packaged (is completed) or not
    list_display = ['__str__', 'is_completed']
    # Except for that normal admin functionality no further adjustments are needed
    pass

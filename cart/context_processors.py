from .cart import Cart
# We import the Cart class


def cart(request):
    # A conext processor is a Python function - in this case cart - that takes an HTTP request object
    # and returns a dictionary that is added to the template context.
    return {'cart': Cart(request)}

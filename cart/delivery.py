from cart.models import DeliveryType
from shop.models import Product

# As we offer different delivery types to choose from, we need a delivery class


class Delivery():

    # The price is initialized with 0, as that is the lowest possible price
    def __init__(self, price=0.00, free_delivery_price=65):
        # The delivery is solely free, if one buys products for 65€ or more after using the voucher code, if one is being used
        self.delivery_name = DeliveryType.objects.get(pk=1).name
        # The delivery has a name and a price which are both shown to the customer
        self.delivery_price = DeliveryType.objects.get(pk=1).price
        # The price is the value of the goods within the users cart
        self.price = price
        # Initially delivery is free is set to false, as usually it is not free, unless certain conditions are met
        self.is_delivery_free = False
        # Only if the free delivery price (65€) is reached, a free delivery offered
        self.set_price_to_get_free_delivery(free_delivery_price)

    def set_delivery(self, id):
        # We need to set the right/chosen delivery type and thus get the deliverys name
        self.delivery_name = DeliveryType.objects.get(pk=id).name
        # If the free delivery is not offered to the customer (see function below),
        if self.is_delivery_free == False:
            # The price of that delivery method is used as the price the customer needs to pay
            self.delivery_price = DeliveryType.objects.get(pk=id).price
            # Otherwise the delivery is free
        else:
            self.delivery_price = 0

    def set_price_to_get_free_delivery(self, amount):
        # If the price is bigger or equal to the specified amount, a free delivery is offered
        if self.price >= amount:
            # We set the is_delivery_free boolean to true, as the order qualifies for free delivery
            self.is_delivery_free = True

    def get_delivery_price(self):
        # We need to get the price of the delivery
        return self.delivery_price

    def get_total_price(self):
        # The total price is the value of the cart plus the delivery price
        return self.price + self.delivery_price

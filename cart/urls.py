from django.urls import path, include
from django.conf.urls import url
from . import views

# The name is used to identify the app
# If one wants to use any function, e.g. cart_add, one needs to write cart:cart_add, instead of simply cart_add
# One benefit of that is that one directly knows where the view function is located, which improves maintainability
app_name = 'cart'

urlpatterns = [
    # The checkout shows the cart details, offers a voucher option, a delivery selection and offers the option to place an order
    path('checkout/', views.cart_detail, name='cart_detail'),
    # Cart add is used to add a selected number of products to the cart
    path('add/<int:product_id>/', views.cart_add, name='cart_add'),
    # Cart_add_one solely adds the product to the cart once (used outside of the product detail page and for the + button in the cart)
    path('add/<int:product_id>/', views.cart_add_one, name='cart_add_one'),
    # Cart remove deletes the product from the cart
    path('remove/<int:product_id>/', views.cart_remove, name='cart_remove'),
    # Cart remove one decreases the quantity by one (though it was implemented in a way, which makes it possible to decrease the quantity by a given amount)
    path('remove_one/<int:product_id>/',
         views.cart_remove_one, name='cart_remove_one'),
    #  Of course, checkout is possible
    path('cart_checkout/<int:id>', views.cart_checkout, name='cart_checkout'),
    # The customer needs to choose a shipping address
    path('choose_address/<int:id>', views.choose_address, name='choose_address'),
    # If the order is complete, a template is shown which summarizes the important information
    path('order_complete/', views.order_complete, name='order_complete'),
    path('order_bank_transfer', views.order_bank_transfer,
         name='order_bank_transfer'),
    # If the user pays via bank transfer, another method is needed, as order_complete works with PayPal
    path('cash_on_delivery', views.cash_on_delivery, name='cash_on_delivery'),
    # If the user wants to collect the products from our Solkraft shop, the procedure differs as well
    path('collection', views.collection, name='collection')

]

from django import forms
# We need the Order and DeliveryType, which we've defined in the models.py file
from .models import DeliveryType, Order

# The customer can add a different quantity of a product to the shopping cart. In this case an integer between 1 to 20.
PRODUCT_QUANTITY_CHOICES = [(i, str(i)) for i in range(1, 21)]


class CartAddProductForm(forms.Form):
    # The quantity is needed and it has to be an integer. Thus, we have to force the quantity to be an integer
    quantity = forms.TypedChoiceField(
        choices=PRODUCT_QUANTITY_CHOICES,
        coerce=int)
    # The update is used to update the quantity in the cart. Initially, no update exists/is needed and thus it is false
    update = forms.BooleanField(required=False,
                                initial=False,
                                widget=forms.HiddenInput)


class CartRemoveProductForm(forms.Form):
    # The customer can delete a product from the cart entirely. The quantity of the product doesn't matter.
    # The product is deleted from the cart regardless of the quantity.
    quantity = forms.TypedChoiceField(
        choices=[(i, str(i)) for i in range(-1, 21)],
        coerce=int)
    # As with the add function we have to ensure that the quantity is indeed an integer


class ChooseDeliveryType(forms.Form):
    # The customer can choose any of the offered delivery types, unless a free delivery is chosen (if the quantity in the cart is high enough)
    delivery_type = forms.ModelChoiceField(queryset=DeliveryType.objects.all(
    ), widget=forms.RadioSelect, required=True, initial=1)
    # Radio buttons are used, since only one option can be chosen


class AddressForm(forms.Form):
    # The address form contains the name, which is the given name or also given names
    name = forms.CharField(label='First name', max_length=100)
    # The surname can consist of one or many names
    surname = forms.CharField(label='Surname', max_length=100)
    # The street and the house number are needed, as the address is used to send the product to the customer
    street = forms.CharField(label='Street', max_length=150)
    number = forms.CharField(label='House number', max_length=10)
    # The city and postal code are needed to ensure that the parcel arrives at the right place
    city = forms.CharField(label='City', max_length=100)
    postal_code = forms.IntegerField(
        label='Postal code', min_value=5, max_value=99999)

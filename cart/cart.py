from decimal import Decimal
from shop.models import Product
from django.conf import settings
from coupons.models import Coupon
from django.core.exceptions import ObjectDoesNotExist


class Cart(object):
    # We need to initialize the cart first
    def __init__(self, request, id=1):
        # Therefore, we need to get the current session, as we work with sessions for the cart to ensure that users can use the cart without being logged in
        self.session = request.session
        # The cart session is defined in the settings.py in the Solkraft folder
        cart = self.session.get(settings.CART_SESSION_ID)
        # The cart is initially empty
        if not cart:
            # Hence, we need to save the empty cart in the session
            cart = self.session[settings.CART_SESSION_ID] = {}
        # The cart cannot only hold the items,
        self.cart = cart
        # but also information about a used coupon
        self.coupon_id = self.session.get('coupon_id')

    def __len__(self):
        # The number of items in the cart is returned in this case, e.g. to show it right next to the cart icon
        return sum(item['quantity'] for item in self.cart.values())

    def __iter__(self):
        # We iterate over all of the items in the cart and get the correct products from the database
        product_ids = self.cart.keys()
        # We get the product objects and add them to the cart
        products = Product.objects.filter(id__in=product_ids)
        # We copy the current cart into the cart variable
        cart = self.cart.copy()
        # And go over all of the products
        for product in products:
            # to add the right products to the cart
            cart[str(product.id)]['product'] = product
            # We go through the information the cart needs, i.e. price and total price
        for item in cart.values():
            # We retrieve the price as a decimal value
            item['price'] = Decimal(item['price'])
            # and the total price is calculated by multiplying the unit price with the quantity
            item['total_price'] = item['price'] * item['quantity']
            # yield works just like return, but it returns a generator object to the caller
            yield item

    # If we add a product to the cart, the quantity isn't updated automatically and usually is one, unless it is specified differently by the user (CartAddProductForm)
    def add(self, product, quantity=1, update_quantity=False):
        # Add a product to the cart or update its quantity, by getting the products id as a string
        product_id = str(product.id)
        # We need to check whether the product is already in the cart;
        if product_id not in self.cart:
            # If it is not then the quantity is 0 and the price is taken to show it correctly once the product gets added to the cart
            self.cart[product_id] = {'quantity': 0,
                                     'price': str(product.return_price())}
        # If the user chooses to update the quantity
        if update_quantity:
            # We set the quantity to the amount the user has chosen, e.g. 1 or 2
            self.cart[product_id]['quantity'] = quantity
            # If the user chooses to add more items to the cart, but already has that specific item in his/her cart, the quantity needs to be added to the current amount
        else:
            # The newly added quantity needs to be saved to the cart
            self.cart[product_id]['quantity'] += quantity
        # Finally the cart is saved - no matter how the actual adjustments look(ed) like
        self.save()

    def remove(self, product):
        # Remova a product from the cart entirely
        product_id = str(product.id)
        # We can only remove products from the cart which are actually in the cart
        if product_id in self.cart:
            # The delete function deletes the product for us - no matter its quantity
            del self.cart[product_id]
            # and finally we save the changes
            self.save()

    def save(self):
        # Mark the session as changed (or modified) to ensure that it is saved.
        self.session.modified = True

    def remove_one(self, product, quantity=1, update_quantity=False):
        # Reduces the quantity of a specific product in the cart by one
        product_id = str(product.id)
        # Of course, only products which are actually in the cart can be deleted
        if product_id in self.cart:
            # If the item quantity is 1 before we do anything, then decreasing the quantity further would lead to 0 products of that type in the users cart
            if self.cart[product_id]['quantity'] == 1:
                # Thus we can delete the product, as we did with the remove function
                del self.cart[product_id]
                # It would be more steps to call the remove function, thus we wrote it here as well
                self.save()
                # If the quantity is not 1, then it must be higher than 1
            else:
                # In that case we can simply decrease the quantity by 1 and save the changes
                self.cart[product_id]['quantity'] -= quantity
                # We decided not to use a static 1, though that would have worked fine, because we want to keep our code easily maintainable and adjustable
                self.save()

    def get_total_price(self):
        # We also want to get the total price of all products in the cart and thus we take the product (result of a multiplication) from price*quantity for all products and add them up
        return sum(Decimal(item['price']) * item['quantity'] for item in self.cart.values())

        # As get_total_price returns a decimal value, but we could need a string in some instances, we've added another function
    def get_total_price_as_string(self):
        # This function calls be one defined above
        price = self.get_total_price()
        # but returns the total price as a string value
        return str(price)

        # We also need to clear the session, e.g. if the user places and order. In that case it wouldn't make sense to leave the items in the cart.
    def clear(self):
        # Thus, we remove the cart from the session
        del self.session[settings.CART_SESSION_ID]
        # and save the changes
        self.save()

    @property
    # @property is used to return a property attribute, which e.g. allows the use of getters and setters
    def coupon(self):
        # If the coupon_id exists,
        if self.coupon_id:
            # it gets returned
            return Coupon.objects.get(id=self.coupon_id)
            # otherwise none is returned instead
        return None

        # The customer wants to get the display and needs it to be visually displayed
    def get_discount(self):
        # If the coupon exists
        if self.coupon:
            # The coupon is returned as the percentage discount times the total price
            return (self.coupon.discount / Decimal('100')) * self.get_total_price()
            # Otherwise a decimal zero is returned, as no coupon was used
        return Decimal('0')

        # Of course, it is nice to show the discount, but we also have to subtract it from the total price.
    def get_total_price_after_discount(self):
        # The new total price is shown and used, if the customer places an order
        return self.get_total_price() - self.get_discount()

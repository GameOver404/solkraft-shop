from celery.schedules import crontab
from .models import Order
# The shared_task annotation can be used for periodic runs/tasks
from celery import shared_task
from django.utils import timezone


@shared_task(run_every=crontab(minute='*/10'))
# We define that the task should run every 10 minutes
def delete_old_orders():
    # As unpaid orders solely stay in our database for a maximum of 7 days, we need to delete the orders which are older than 7 days.
    #  Thus, we filter through all of the objects which are not confirmed, as those are the ones which are not paid yet.
    orders = Order.objects.filter(
        is_confirmed=False, is_cash_on_delivery=False)
    # We loop through all of the orders in our datebase.
    for order in orders:
        # If the expiration date is smaller than the current time, that means that more than 7 days have passed, since the order was placed.
        if order.expiration_date < timezone.now():
            # Hence, we delete all of the orders which are unpaid and older than 7 days.
            order.delete()
    # To inform the admin that the task has finished successfully, a message is printed to the console, which states when the deletion process was finished.
    print("Das Löschen der Objekte ist abgeschlossen: {}".format(timezone.now()))

import logging
import paypalrestsdk
from Solkraft import settings
from paypalrestsdk import Sale

# logging configuration for PayPal payments
logging.basicConfig(level=logging.INFO)

# The ID and SID are defined in the settings and for security reasons the actual data is saved in the .env file
ID = settings.PAYPAL_ID
SID = settings.PAYPAL_SID

# We created a PaypalPayment class in which we defined the adjusted PayPal REST SDK methods and added a refund option


class PaypalPayment():
    # We need to initiate the payment
    def __init__(self, request, total_price):
        # We need to set the right price, as otherwise the price would be set to 0.01€
        self.price = str(total_price)
        self.request = request
        # We need to cinfigure the Rest SDK to be able to use it
        paypalrestsdk.configure({
            "mode": "sandbox",  # sandbox or live, but live would actually transfer money
            "client_id": ID,
            "client_secret": SID
        })
# The payment needs to be specified
# This includes the redirect URL, if the customer pays the product he/she gets redirected to the order complete site, otherwise the user gets redirected to the front page
        self.payment = paypalrestsdk.Payment({
            "intent": "sale",
            "payer": {
                "payment_method": "paypal"},
            "redirect_urls": {
                "return_url": "http://127.0.0.1:8000/cart/order_complete",
                "cancel_url": "http://127.0.0.1:8000/"},
            "transactions": [{
                "item_list": {
                    "items": [{
                        "name": "Your purchase from Solkraft",
                        "sku": "Purchase",
                        "price": self.price,
                        "currency": "EUR",
                        "quantity": 1}]},
                "amount": {
                    "total": self.price,
                    "currency": "EUR"},
                "description": "Your purchase from Solkraft"}],
        })

    def make_payment(self):
        # The payment must be created (standard function from PayPal REST SDK)
        if self.payment.create():
            print("Payment created successfully")
        else:
            print(self.payment.error)

    def authorize_payment(self):
        # Authorization of the payment (standard function from PayPal REST SDK)
        for link in self.payment.links:
            if link.rel == "approval_url":
                approval_url = str(link.href)
                print("Redirect for approval: %s" % (approval_url))
                return (approval_url), self.payment.id
        return None


def execute_payment(request):
    # Excecution of the payment (standard function from PayPal REST SDK)
    payment_id = str(request.GET.get('paymentId', None))
    payer_id = str(request.GET.get('PayerID', None))
    payment = paypalrestsdk.Payment.find(payment_id)
    if payment.execute({"payer_id": payer_id}):
        return True
    else:
        return False


def make_refund(pay_id):
    # To make a refund the REST SDK needs to be configured - just as it is the case with the payment
    paypalrestsdk.configure({
        "mode": "sandbox",  # sandbox or live, but live would actually transfer money
        "client_id": ID,
        "client_secret": SID
    })
    # The correct payment needs to be found
    payment = paypalrestsdk.Payment.find(pay_id)
    # The correct sales id needs to be retrieved
    sale_id = payment.transactions[0].related_resources[0].sale.id
    # As the sales id is needed to find the right Sale object
    sale = Sale.find(sale_id)
    # Afterwards a refund can be issued to the person who was granted the refund
    refund = sale.refund({})
    # If the refund was executed successfully,
    if refund.success():
        # a success message is printed to the console.
        print("Rückerstattung[%s] erfolgreich" % (refund.id))
    else:
        # Otherwise the exception is printed to the console.
        raise Exception('Rückerstattung nicht möglich: %s' % refund.error)

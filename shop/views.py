from dal import autocomplete
from django.http import HttpResponse
from .models import Category, Product, Review, Description, Parameter, ExtraPhoto, Wishlist
from .forms import ReviewForm
from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, ListView
import random
from cart.forms import CartAddProductForm
from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.contrib.auth.models import User
from django.template.loader import render_to_string
# The Q objects allow for more complex queries, as the Django Object Relational Mapper (ORM) is great but somewhat limited regarding complex queries
from django.db.models.query_utils import Q
from django.http import HttpResponseRedirect
from django.contrib.auth.forms import PasswordResetForm
from django.core.mail import EmailMessage, BadHeaderError
from statistics import mean
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from Solkraft import settings
import operator
from collections import OrderedDict
from django.core.paginator import Paginator


def product_list(request, category_slug=None, page=1):
    # Initially we start with the first page
    category = None
    # We get all the categories and available products we have
    categories = Category.objects.all()
    # As we can say that a product isn't available as of now, but in that case there's no need to display it
    product_list = Product.objects.filter(available=True)
    # We create a dictonary to get all the relevant products
    dictionary = None
    dictionary = {}
    for product in product_list:
        # We loop through all available products and filter for the specific product to get its parameters
        parameters = Description.objects.filter(product=product)
        # and add them to the dictionary
        dictionary[product] = parameters
    if category_slug:
        # If a category was passed, we get the specific category
        category = get_object_or_404(Category, slug=category_slug)
        # and solely display the products from said category and not from every category
        product_list = product_list.filter(category=category)
        # We use pagination to ensure that the loading times are well managed
    paginator = Paginator(product_list, 30)
    # Thus, we have to add pages,
    products = paginator.get_page(page)
    # so that the user can see all of our products with ease
    return render(request, 'shop/product/list.html',
                  {'category': category, 'categories': categories, 'products': products, 'dictionary': dictionary, 'paginator': paginator})


@login_required(login_url='account:login')
# If a user is not logged in, he/she is redirected to the login page
def add_review(request, new_review, product):
    review_form = ReviewForm(data=request.POST)
    # We get the review_form and check whether it is valid
    if review_form.is_valid:
        new_review = review_form.save(commit=False)
        # If it is we ensure that the review is published for the right product
        new_review.product = product
        # and from the right user.
        new_review.user = request.user
        # and then we will save the review to the database
        new_review.save()
    return new_review, review_form


def reviews_by_user_count(request, product):
    # We can get the number of reviews for a single product
    return Review.objects.filter(user=request.user, product=product).count()


def terms_view(request):
    # simply loads the static file
    return render(request, 'about/terms_of_service.html')


def product_detail(request, id, slug):
    # every user is allowed to see the product details, as long as the prouct is available
    product = get_object_or_404(Product, id=id, slug=slug, available=True)
    descriptions = Description.objects.all()
    # We need to get the description for that specific product
    descriptions = descriptions.filter(product=product)
    # The user can add the product to his/her cart and also choose the amount
    cart_product_form = CartAddProductForm()
    # We get all the reviews for that specific product
    reviews = product.review.filter(active=True)
    # Though only those which are set to active
    new_review = None
    users_reviews = None
    if request.user.is_authenticated:
        # We need to get the number of reviews the currently logged in user has written for said product
        users_reviews = reviews_by_user_count(request, product)
        # If the user writes a review and there is no review from him/her yet,
    if request.method == 'POST' and users_reviews < 1:
        # She/he is allowed to post a new review
        new_review, review_form = add_review(request, new_review, product)
    else:
        # Otherwise the review form is displayed, so that the user is able to add an review, unless one was alreay aded, which is then instead shown in its place
        review_form = ReviewForm()
    # We show 4 recommended products,
    recommendations = choose_recommended(request, product.category, product, 4)
    # as well as 4 similar products
    similar = choose_similar(request, product, product.category, 4)

    if not 'recent' in request.session or not request.session['recent']:
        # If the user hasn't viewed any products previously or there is no information about that, then of course no recently viewed products can be shown to him/her
        request.session['recent'] = [id]
        show_recent = None
    # Otherwise, the user is shown up to 4 recently viewed products
    else:
        # We define what we mean by recent which is the recent session data
        recent = request.session['recent']
        # and get all of the products which the user has viewed recently
        show_recent = [Product.objects.get(id=id)
                       for id in recent]
        # If we find the id in the recent session data
        if id in recent:
            # We remove it
            recent.remove(id)
        # and add it back to the session data with a slight modification,
        recent.insert(0, id)
        # afterwards we check the length of our recent session data
        if len(recent) > 4:
            # the pop method returns the last item
            recent.pop()
            # Thus we get a new recent request session
        request.session['recent'] = recent
        # Potentially additional photos were added to the product, as the product model itself only holds one item
    extra_photos = ExtraPhoto.objects.filter(product=product)
    # The additional photos are of course also shown to the user and thus our context is rather big
    context = {'product': product,
               'extra_photos': extra_photos,
               'cart_product_form': cart_product_form,
               'reviews': reviews,
               'new_review': new_review,
               'review_form': review_form,
               'descriptions': descriptions,
               'users_reviews': users_reviews,
               'recommendations': recommendations,
               'similar': similar,
               'recent': show_recent, }
    return render(request, 'shop/product/detail.html', context)


def password_reset_request(request):
    # The user can reset the password from the login page if he/she has forgotten the password
    # or from the profile, if he/she wants to change the password
    if request.method == "POST":
        # If the form is posted by the user, we get the information from the form
        password_reset_form = PasswordResetForm(request.POST)
        # check whether the form is valid
        if password_reset_form.is_valid():
            # an if it is we retrieve and clean the email data
            data = password_reset_form.cleaned_data['email']
            # and get the associated user. We do so with the help of Q objects which allow for more complex queries.
            associated_users = User.objects.filter(Q(email=data))
            # If the associated user doesn't exist, we actually do nothing, as the user is informed after sending the mail that
            # he/she should try again and ensure that the entered email is correct and after all if nothing works, they can always write Solkraft - be it via email or through the contact form
            if associated_users.exists():
                for user in associated_users:
                    # If the user does exist, we create a email for him/her
                    subject = "Zurücksetzen Ihres Solkraft Passworts"
                    # We actually take a email template in this case, instead of writing the text here
                    email_template_name = "password/password_reset_email.txt"
                    # c contains the entire conext, e.g. the email-address we will send the mail to, our omain, the site name, the base64 encoed primary key of the user
                    # the user itself and a token which was created to check that the owner of the email address and the user match, as well as the used protocol
                    c = {
                        "email": user.email,
                        'domain': 'Solkraft.PythonAnywhere.com',
                        'site_name': 'Solkraft',
                        "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                        "user": user,
                        'token': default_token_generator.make_token(user),
                        'protocol': 'https',
                    }
                    # We render all of this information into a string for the mail content
                    email = render_to_string(email_template_name, c)
                    try:
                        # and try to create and send the email message
                        email = EmailMessage(subject, email, settings.CONTACT_EMAIL,
                                             [user.email])
                        email.send()
                        # We need to catch the potential bad header
                    except BadHeaderError:
                        # and create a response to let us know that something went wrong
                        return HttpResponse('Invalid header found.')
                        # If everything went according to plan, the user is redirected to a page which informs them on what to do next
                    return redirect("account/password_reset_done/")
    # If there was no post request yet, we have to return the PasswordResetForm as context to our page
    password_reset_form = PasswordResetForm()
    return render(request=request, template_name="password/password_reset_form.html", context={"password_reset_form": password_reset_form})


@ login_required
# To delete a review one must be logged in as well. However, in this case no redirect is necessary as this information is not shown to users who aren't logged in
def delete_review(request, id):
    # We get the specific review the user wants to delete
    review = get_object_or_404(Review, id=id)
    # The review was written for a specific product, which needs to be stripped of one review
    product_id = review.product.id
    product_slug = review.product.slug
    # We delete the review from the database
    review.delete()
    # once the review is deleted, the user gets referred back to the same site he/she came from
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class ParameterAutocomplete(autocomplete.Select2QuerySetView):

    def get_queryset(self):
        # Autocomplete is possible and thus we get all of the descriptions
        qs = Description.objects.all()

        if self.q:
            # and fill in the rest of the description automatically
            qs = qs.filter(parameter__name__startswith=self.q)
        # Of course that autocomplete must be returned as well
        return qs


def about_view(request):
    # A static website which informs the user about who Solkraft actually is and why we exist (well hypothetically at least)
    return render(request, 'about/about_us.html')


def privacy_view(request):
    # Our privacy policy is also a simple static web page
    return render(request, 'about/privacy_policy.html')


def choose_recommended(request, category, current_product, amount):
    # We show a total of four product recommendations to the user, which are from the same category
    # However, we do not want to show the user the same product which is currently shown in the recommendations
    products = Product.objects.filter(
        category=category).exclude(pk=current_product.pk)
    dict = {}
    # We loop through all of the products of the same category as the currently displayed product
    for product in products:
        # We only show products which hav an average rating, which is bigger or equal to the median (3)
        if product.average_rating != None and product.average_rating >= 3.0:
            # Such products are added to the dictonary
            dict[product] = product.average_rating
    # We sort the dictonary and reverse it - now we do not have too many products and it won't matter, but later on it could be beneficial, as we show products on higher pages
    dictionary = OrderedDict(
        sorted(dict.items(), key=operator.itemgetter(1), reverse=True))
    # and as we only want to display four product recommendations and not all products with at least median average rating score:
    recommended_products = []
    i = 0
    for product in dictionary:
        # We add one product at a time to the newly created dictonary
        recommended_products.append(product)
        # We loop through the dictonary until the requested amount (in our case four) is reached
        i += 1
        # Then we break out of the for loop
        if (i == amount):
            # The break ensures that we break out of the for loop
            break
        # to return the dictonary for display
    return recommended_products


class WishListView(ListView):
    model = Wishlist
    # The wishlist list view uses the wishlist model and wishlist.html
    template_name = 'shop/product/wishlist.html'
    # We paginate our wishlist by a total of 10 items, though that can easily be changed here
    paginate_by = 10
    context_object_name = 'wishlist'

    def get_queryset(self):
        # We filter by user, if the user is logged in to only get his/her wished items
        # We cannot not return a list and @login_required isn't known by the ListView. However, under rare circumstances a user might lands on the wishlist without being logged in
        if self.request.user.is_authenticated == False:
            # Certainly it is not a good idea to show an error message when we can do something about it. So, if an anonymous user lands on the wishlist he/she sees the wishlist of all users
            # which isn't problematic as we have pagination and thus no problems will occur as the data isn't confidential at all and no information about the users can be retrieved from seeing all wished items
            return Wishlist.objects.all()
        # else:
            # We return every wished_item from the currently logged in user, which is the reason why the user needs to be logged in to use the wishlist
        return Wishlist.objects.filter(user=self.request.user)


@ login_required(login_url='account:login')
def add_to_wishlist(request, product_slug):
    # Once can only add something to the wishlist if he/she is logged in, otherwise he/she will have to log in
    item = get_object_or_404(Product, slug=product_slug)
    # We get the item and create the wished_item with the gathered information
    wished_item, created = Wishlist.objects.get_or_create(
        wished_item=item, slug=item.slug, user=request.user,)
    # After adding a product to the wishlist, the user is redirected to his/her wishlist
    return redirect('shop:wishlist')


@ login_required(login_url='account:login')
def delete_from_wishlist(request, product_slug):
    # If someone wants to delete a product from the wishlist and is not logged in, he/she will get redirected to the login page
    customer = request.user
    # we search for the item the user wants to delete from his/her wishlist
    delete_item = Wishlist.objects.filter(user=customer, wished_item=Product.objects.get(
        slug=product_slug))
    # If the item is not in the users' wishlist, we simply redirect the user to his/her wishlist
    if delete_item is None:
        return redirect('shop:wishlist')
    else:
        delete_item.delete()
        # Otherwise we delete the item from the users' wishlist and redirect the customer to his/her wishlist afterwards
        return redirect('shop:wishlist')


def levenshtein(a, b):
    # algorithm for calculating the word distance. This is indeed not our own work, but it still works like a charm
    n, m = len(a), len(b)
    # Calculates the Levenshtein distance between a and b.
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a, b = b, a
        n, m = m, n
    # Seeing that O(min(n,m)) above - the Levenshtein distance has a Big-O notation of O(n*m)
    # with n being the length of one string and m being the length of the other string
    current = range(n + 1)
    for i in range(1, m + 1):
        previous, current = current, [i] + [0] * n
        for j in range(1, n + 1):
            add, delete = previous[j] + 1, current[j - 1] + 1
            change = previous[j - 1]
            if a[j - 1] != b[i - 1]:
                change = change + 1
            current[j] = min(add, delete, change)

    return current[n]


def choose_similar(request, current_product, category, amount):
    # The product (detail) page also has a section where similar products are shown to the user
    current_product_parameters = Description.objects.filter(
        product=current_product)
    # We retrieve the descriptions for the product and
    # as we know the current product, we do not need to show it as a similiar product - after all it is the SAME product
    correct_products = Product.objects.filter(
        category=category).exclude(pk=current_product.pk)
    # We initialize a empty dictionary which is filled in the below for loop
    dict = {}
    # We loop through all of the products which have the same category, but are not the same product
    for product in correct_products:
        # Initially we set it to 0, as we cannot work with a empty dictonary entry
        dict[product] = 0
        # Then we also loop through the parameters of each potentially similar product
        for parameter in current_product_parameters:
            # We get the description of the product
            description = Description.objects.filter(
                product=product, parameter=parameter.parameter)
            # If the products parameters are equal to the currently viewed products parameters, the product is added to the dictionary
            if description == current_product_parameters:
                dict[product] = product
            else:
                # We need to define the other_product_parameter before using it
                other_product_parameter = ""
                # and check the product with the levenshtein function to get the word distance
                dict[product] += levenshtein(parameter.description,
                                             other_product_parameter)

    # We sort the dictionary, before actually using it to loop through all of its items
    dictionary = OrderedDict(
        sorted(dict.items(), key=operator.itemgetter(1), reverse=True))
    # We initialize a empty list which will be filled in the for loop
    similar_products = []
    # We want to show a specified number of similiar products
    i = 0
    # and thus loop through all of the products in the dictionary
    for product in dictionary:
        # and append the products one by one
        similar_products.append(product)
        # but solely  as many as we need
        i += 1
        # which in our case is four similar products
        if i == amount:
            # The break ensures that we break out of the for loop
            break
    # and afterwards we return the list which contains a maximum of four items in our case
    return similar_products


def summary_view(request):
    # This static page holds information about the offers of Solkraft and has a option to enter oneself as a participant of the (online) plant workshops
    return render(request, 'about/summary.html')


def searching(request, page=1, phrase=None):
    # The first page is, of course, page 1
    dictionary = None
    # However, potentially there are more pages than solely the first one
    paginator = None
    # Only if the user actually enters some text, the search is carried out
    if request.method == 'GET' and phrase is None:
        # We get the phrase the user has entered
        phrase = request.GET["search_phrase"]
        # Check whether it is actually a text
        if len(phrase) < 1:
            # Otherwise it is nothing
            phrase = None
            # However, usually users enter some text to search for a specific product
    if phrase is not None:
        # We loop through the entire product list to see if we have any available products which match the search phrase
        product_list = Product.objects.filter(available=True,
                                              name__icontains=phrase)
        dictionary = {}
        # We loop through all products which fit the search phrase
        for product in product_list:
            # Get their parameters
            parameters = Description.objects.filter(product=product)
            # and add them to a dictionary
            dictionary[product] = parameters
            # We paginate the search by 9, which means three rows will be shown to the customer
        paginator = Paginator(product_list, 9)
        # Currently the search doesn't yield that many results, but it could later on an thus we've decided to add the pagination
        products = paginator.get_page(page)
    else:
        # If no matching product can be found, then no product is shown to the customer
        products = None
        # Either way the customer receives a response anytime he/she presses the search button
    return render(request, 'shop/product/searching.html', {'products': products, 'dictionary': dictionary, 'paginator': paginator, 'phrase': phrase})


def policy_view(request):
    # The shipping policy is another static information page
    return render(request, 'about/shipping_policy.html')


def random_products(request, category_slug=None):
    # In our homepage (initial page), we wanted to show four random items
    products = Product.objects.all().values('pk')
    # We get all the products primary keys
    list = []
    # and loop through all the product we've found
    for product in products:
        # appending all of the primary keys to the list
        list.append(product['pk'])
    if len(list) < 4:
        # if the length of the list is smaller than 4 we take a random sample and put it at the place of the length of the list
        sample = random.sample(list, len(list))
    else:
        # If the length of the list is 4, we do the same, but we can simply write the 4 in that case
        sample = random.sample(list, 4)
        # and initialize a new list
    random_products = []
    # We had to define the sample in the else block as well, as otherwise Django returned an error that the variable sample was used before its assignment
    for pk in sample:
        # We take the primary key of the products
        product = Product.objects.get(pk=pk)
        # and append it to the random_products list
        random_products.append(product)
    # We initialize the category as none
    category = None
    # and retrive all categories afterwards
    categories = Category.objects.all()
    # If a category slug exists
    if category_slug:
        # We try to retrieve the category for that product
        category = get_object_or_404(Category, slug=category_slug)
    # We return the 4 randomly chosen products and categories
    return render(request, 'shop/main.html', {'category': category, 'categories': categories, 'products': random_products})

from dal import autocomplete
from .models import Review, Parameter, Description, Product, ExtraPhoto
from django import forms
from django.contrib.auth.models import User


class ReviewForm(forms.ModelForm):
    # Meta means that we use the information from another already existing model, instead of creating new fields for the form which would be the same in this case
    class Meta:
        # The review forms takes its fields from the Review model (located in the models.py)
        model = Review
        # and we solely take the fields body and rating, as body is the text the user writes and the rating is the number of stars
        fields = ('body', 'rating', )


class DescriptionForm(forms.ModelForm):
    # As we only have to change the meta information in the Description model, if we want to change e.g. the form, the maintainability is improved by using Meta
    class Meta:
        # The description form takes all the fields from the description model
        model = Description
        fields = ('__all__')
        # we use autocomplete to autocomplete the descriptions (method located in views.py)
        widgets = {
            'parameter': autocomplete.ListSelect2(url='parameter-autocomplete', forward=['product']
                                                  )
        }

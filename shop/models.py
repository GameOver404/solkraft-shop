from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
# returns the average value
from django.db.models import Avg
from decimal import Decimal
from django.core.validators import MinValueValidator


class Category(models.Model):
    # The category model contains the name of the category
    name = models.CharField(max_length=200, db_index=True)
    # as well as its slug, which is unique as it is used to identify the category and to interact/"load" it
    slug = models.SlugField(max_length=200, db_index=True, unique=True)

    class Meta:
        # We order it by its name
        ordering = ('name',)
        # and call it category
        verbose_name = 'category'
        # and the plural categories
        verbose_name_plural = 'categories'

    def __str__(self):
        # We return the name of the category
        return self.name

    def get_absolute_url(self):
        # and get the product list by category, if a category was selected
        return reverse("shop:product_list_by_category", args=[self.slug])


class Product(models.Model):
    # The product belongs to a category
    category = models.ForeignKey(
        Category, related_name='products', on_delete=models.CASCADE)
    name = models.CharField(max_length=200, db_index=True)
    # has a name and slug to identify it
    slug = models.SlugField(max_length=200, db_index=True)
    # One image (the one which is shown initially in the carousel in the product detail page and to make it easier to find certain pictures, the date of the upload is used with folders, starting from the year to month to day)
    image = models.ImageField(upload_to='products/%Y/%m/%d', blank=True)
    description = models.TextField(blank=True)
    # set the min value to one cent, as we do not hand out free samples on a regular basis
    price = models.DecimalField(max_digits=10, decimal_places=2, validators=[
        MinValueValidator(Decimal('0.01'))])
    # The cheapest possible price is one cent and if a product is not available, it is not shown in the product list
    available = models.BooleanField(default=True)
    # We save the information when the product was created
    created = models.DateTimeField(auto_now_add=True)
    # and the information on when the product was updated
    updated = models.DateTimeField(auto_now=True)
    # If we do not produce the item itself, we have a supplier/producer
    producer = models.CharField(max_length=100, blank=True)
    # The new price is either zero, which means that the price was not changed or a new price can be entered to show it to the user,
    # along with the information on the discount in percent
    new_price = models.DecimalField(max_digits=10, decimal_places=2, validators=[
        MinValueValidator(Decimal('0.00'))], default=0)

    class Meta:
        # We order the products by name, as we usually added products of the same category "in bulk"
        ordering = ('name',)
        # The indexing is made with id and slug
        index_together = (('id', 'slug'),)

    @property
    def average_rating(self):
        # The product has an average rating
        return Review.objects.filter(product=self).aggregate(Avg('rating'))['rating__avg']

    @property
    def sales_percent(self):
        if self.new_price != 0:
            # If a new price was set, we show the user the discount in percent
            return 100 - (int(self.new_price / self.price * 100))
        return 0

    def return_price(self):
        if self.new_price != 0:
            # If a new price was set, then that is the one the user has to pay
            return self.new_price
            # Otherwise, of course, the old/standard price must be paid
        return self.price

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        # the absolute url is created with the help of the id and slug, as they are used to identify the product
        return reverse("shop:product_detail", args=[self.id, self.slug])


class Review(models.Model):
    # A review is always created for a specific product
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name='review')
    # One can choose between 1-5, where 1 is the worst and 5 is the best possible rating
    # The rating choice is displayed with stars in the template
    RATING_CHOICES = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5'),
    )
    # A user can only write one review for a specific product
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # The default is set to 3, as it is the median value
    rating = models.IntegerField(choices=RATING_CHOICES, default=3)
    # The body is the text the user writes and usually it isn't too long, but we want to give our customers the opportunity to add more information if they like
    body = models.TextField(max_length=1000)
# We save the information on when the review was created
    created = models.DateTimeField(auto_now_add=True)
    # and if the review gets updated, this information is saved as well
    updated = models.DateTimeField(auto_now=True)
    # Only active reviews are shown to our customer. Staff can e.g. set reviews to inactive, if things need to be clarified or foul language was used etc.
    active = models.BooleanField(default=True)

    class Meta:
        # We order by the date the rating was created and if there are more ratings created on the same date, we take the rating (stars) into account as well
        ordering = ('created', 'rating',)

    def __str__(self):
        # We return a string with the important information: who created a review for what
        return 'Benutzerbewertung {} des Produkts {}'.format(self.user, self.product)


class Parameter(models.Model):
    # A parameter, e.g. care instructions for seeds, always belongs to a certain category
    category = models.ForeignKey(
        Category, related_name='parameters', on_delete=models.CASCADE)
    # and has a namen which is set as the database index, as it is unique and used for (useful) lookups
    name = models.CharField(max_length=100, db_index=True)

    def __str__(self):
        return self.name


class Wishlist(models.Model):
    # Every user can create his/her own wishlist, which is initially empty
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # The wished_item is the product the user adds to his/her wishlist
    wished_item = models.ForeignKey(Product, on_delete=models.CASCADE)
    # It uses a slug to work with the model
    slug = models.CharField(max_length=30, null=True, blank=True)
    # and the information on when the product was saved to the wishlist is also saved
    added_date = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-added_date', ]


class Description(models.Model):
    # The parameters are needed for the description, as they describe the product to some extend
    parameter = models.ForeignKey(
        Parameter, related_name='description', on_delete=models.CASCADE)
    # We couldn't use description again as a related_name and thus used the abbreviation descript
    product = models.ForeignKey(
        Product, on_delete=models.CASCADE, related_name='descript')
    # Since we did use description for the actual description
    description = models.CharField(max_length=200)

    def clean(self):
        # One can only use a parameter, if the parameter was set for the given category
        if not self.parameter.category == self.product.category:
            # This ensures that our staff doesn't accidentally choose a wrong parameter
            raise ValidationError(
                'Die Produkt- und Parameterkategorien sind nicht kompatibel.')

    def __str__(self):
        # Of course, we can also return the parameters for a certain product with the associated description
        return 'Parameter {} von Produkt {} mit Beschreibung {}'.format(self.parameter, self.product, self.description)


# Class to add more than one photo to a product
class ExtraPhoto(models.Model):
    # One prodcut can have as many pictures as one pleases
    product = models.ForeignKey(
        Product, related_name='extra_photo', on_delete=models.CASCADE)
    # All images were either taken from Pixabay or (for the most part) own pictures were used, which were specifically taken for this project
    image = models.ImageField(upload_to='products/%Y/%m/%d', blank=True)

    def __str__(self):
        # It returns the id of the extra photo and the product name
        return str(self.id) + ": " + self.product.name

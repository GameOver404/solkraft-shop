from django.contrib import admin
from django.urls import path, re_path
from django.conf.urls import url
from . import views
from django.views.generic import TemplateView

# The name is used to identify the application
# If one wants to use any function, e.g. add_to_wishlist, one needs to write shop:add_to_wishlist, instead of simply add_to_wishlist
# One benefit of that is that one directly knows where the view function is located, which improves maintainability
# Another benefit is that the same name can be given to another path in a different application
app_name = 'shop'

urlpatterns = [
    # Our staff works with the admin panel for the most part, though they also have their dedicated app with templates/function specifically defined to make their work easier for them
    path('admin/', admin.site.urls),
    #     In the home page 4 random products are displayed (they are different every time you'll load the page)
    path('', views.random_products, name='random_products'),
    #     We added some static pages for information, e.g. this about us and the below terms of service page
    path('about_us.html', views.about_view, name='about_us'),
    path('terms_of_service.html', views.terms_view, name='terms_of_service'),
    #     A wishlist with all needed functions was created as well
    path('wishlist/', views.WishListView.as_view(), name='wishlist'),
    #     The product can be added from the product page and the product cards in the product_list, random_products and search
    path('add_to_wishlist/<slug:product_slug>',
         views.add_to_wishlist, name="add_to_wishlist"),
    #     And the wishlist can be deleted in the product page and inside the wishlist
    #     If a user tries to delete an item from the list, which isn't in his/her wishlist, the user is redirected to the wishlist without any changes
    path('delete_from_wishlist/<slug:product_slug>', views.delete_from_wishlist,
         name='delete_from_wishlist'),
    #     More static sites
    path('privacy_policy.html', views.privacy_view, name='privacy_policy'),
    path('shipping_policy.html', views.policy_view, name='shipping_policy'),
    #     The summary contains information about the offered services and allows the user to register for the plant workshops
    path('summary.html', views.summary_view, name='summary'),
    #     The products are displayed in a list and pagination exists which ensures that our website doesn't take too long to load
    path('products/', views.product_list, name='product_list'),
    path('<int:page>/', views.product_list, name='product_list'),
    path('<slug:category_slug>/', views.product_list,
         name='product_list_by_category'),
    #     The category slug is needed to ensure that only the products of a specific category are shown, if a category is selected by the user
    path('<slug:category_slug>/<int:page>/',
         views.product_list, name='product_list_by_category'),
    #     Of course every item has its dedicated page with further information about it
    path('<int:id>/<slug:slug>/', views.product_detail, name='product_detail'),
    #     Our user can add reviews, but they can also delete their own reviews
    path('delete_review/<int:id>', views.delete_review,
         name='delete_review'),
    #     A search was implemented as well. It isn't too smart, but therefore it wasn't simply taken from an existing source but created by ourselves
    #     We had to use re_path, since we had to add the dollar sign at the end to ensure that the search works well for every type of user input
    re_path(r'^searching/<str:phrase>/$',
            views.searching, name='searching'),
    #   Pagination shouldn't be needed for the search as of now, but potentially the webshop will add more products later on and someday it might very well be needed
    path('searching/<int:page>/<str:phrase>',
         views.searching, name='searching'),
]

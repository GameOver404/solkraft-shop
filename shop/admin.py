from django.contrib import admin
from .forms import DescriptionForm
from .models import Category, Product, Description, ExtraPhoto, Review, Parameter, Wishlist


class ParameterInline(admin.StackedInline):
    # StackedInline offers the ability to edit models on the same page as the parent model
    model = Parameter
    # and it wouldn't make sense to let our staff change the page to add this information


class DescriptionAdmin(admin.StackedInline):
    # We want to edit the parameter and description on the Product site, as those models are important for our product
    model = Description
    # and it is way easier to have all the needed information on one page to ad and edit it


@admin.register(Product)
# The annotation above is used to add the Product model to the Django admin, so that data for those models can be created, deleted, updated and queried through the UI
class ProductAdmin(admin.ModelAdmin):
    # We display the relevant data from the product, where new_price is either a new price or 0, if the price should be used, i.e. there is no discount on the product
    list_display = ['name', 'slug', 'new_price',
                    'price', 'available', 'created', 'updated']
    # The admin can filter the Product, to e.g. only get the available proucts or those created at a certain time
    list_filter = ['available', 'created', 'updated']
    # new_price and available can be easily edited, as discounts/offers change and the availability can change as well
    list_editable = ['new_price', 'available']
    # The prepopulated field is used to fill in the slug, when writing the name of the product,
    # so that the admin doesn't have to fill it in manually - though changes can be made, if one likes to adjust the slug
    prepopulated_fields = {'slug': ('name',)}
    # The descritpion admin is shown as an inline model
    inlines = [DescriptionAdmin]


@admin.register(Category)
# The admin should also be able to create, update, delete and query the Category model
class CategoryAdmin(admin.ModelAdmin):
    # The name an slug are relevant in this case
    list_display = ['name', 'slug']
    # and as with the product the slug is filled in automatically, if the admin adds the name
    prepopulated_fields = {'slug': ('name',)}
    # The parameters are shown as well, which is possible due their model being classified as StackedInline
    inlines = [ParameterInline]


@admin.register(Wishlist)
# The wishlist can be modified by our admins as well, though that usually shouldn't be necessary
class WishlistAdmin(admin.ModelAdmin):
    # They can see (and edit) all of the fields from the Wishlist model
    list_display = [field.name for field in
                    Wishlist._meta.get_fields()]


@admin.register(Review)
# We also need to register the Reviews, which will rarely be modified by our staff
class ReviewAdmin(admin.ModelAdmin):
    # It is mostly the body itself, which could be edited to delete foul language
    list_display = ['body', ]
    # They can also search for a rating, if they'd like (or rather need) to adjust it
    search_fields = ['body', ]
    # Of course, our staff would never delete bad ratings! ;) Now I wonder how many shops do that... Honestly potentially most of them
    list_filter = ['rating', ]


@admin.register(ExtraPhoto)
# Our staff needs to add and edit extra photos as well
class PhotoAdmin(admin.ModelAdmin):
    # Since usually we would like to show more than just one picture and certainly the images can be improved over time
    pass

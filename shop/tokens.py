from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.utils import six


class TokenGenerator(PasswordResetTokenGenerator):
    # We create a hash value with the help of django-utils-six
    # It is not only used as a password reset token as the name implies, but also for the account activation
    def _make_hash_value(self, user, timestamp):
        return (
            six.text_type(user.pk) + six.text_type(timestamp) +
            six.text_type(user.is_active)
        )


# At least we did use it for this purpose as well
account_activation_token = TokenGenerator()

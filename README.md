# Solkraft Shop

This webshop was created with Django and Bootstrap + MDB (Material Design for Bootstrap)

It was deployed on PythonAnywhere and can be found here: https://solkraft2.pythonanywhere.com/ 

## Prerequisites

This project requires the following dependencies to be installed beforehand:

* Python and pip
* GTK+  

As we use Cairo to create the invoice as a PDF-document, one needs to install the GTK+:

The GTK+ for Windows Runtime Environment can be installed directly from GitHub: https://github.com/tschoonj/GTK-for-Windows-Runtime-Environment-Installer

The installation file for Linux, MacOS and Windows (though for the latter we recommend to use the link above) can be downloaded from GTK's official website: https://www.gtk.org/

## Django Dependencies

* Django
* CairoSVG
* PayPal-REST-SDK
* WeasyPrint
* Django-Markdown2
* Bleach

To install all the needed dependencies run:

```
pip install -r requirements.txt
```

## Usage

* Once the dependencies are installed (via pip) start the app with:

```
python manage.py runserver
```

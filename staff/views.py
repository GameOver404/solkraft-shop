from cart.paypal_payment import *
from django.core.mail import send_mail
from Solkraft.settings import CONTACT_EMAIL

from io import BytesIO

import weasyprint
from django.core.mail import EmailMessage
from django.shortcuts import render
from cart.models import Order, OrderComponent
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import render, get_object_or_404, redirect
from django.template.loader import render_to_string
from Solkraft import settings
from .forms import OrderButtons, FilterButton, AddDeliverySearchingCode
from django.http import HttpResponseRedirect, HttpResponse
from django.utils import timezone
from customer.models import Complaint


def are_components_completed(components):
    # checks whether order components are complete
    if components != None:
        for component in components:
            # If they are not none, we loop through all of them and check whether the parcel was packed with them
            if component.is_completed == False:
                # If at least one component isn't complete/packaged, the parcel isn't ready to be shipped
                return False
    # Otherwise, the parcel is complete and ready to be shipped
    return True


@staff_member_required
def manage_orders(request):
    # Only staff members are allowed to manage orders
    orders = Order.objects.all()
    for order in orders:
        # We loop through all orders and check whether the order should be deleted, as it wasn't paid within 7 days or has no products left
        if (order.is_confirmed == False and order.expiration_date < timezone.now()):
            order.delete()
    orders = Order.objects.all()
    if request.method == 'POST':
        # If one fills out a form or rather clicks on th OrderButtons, we need to check whether the form is valid or not
        form = OrderButtons(request.POST)
        if form.is_valid():
            val = form.cleaned_data.get("btn")
            # Then we have to check which button was pressed
            if val == 'Paid orders':
                # If our staff member choose paid orders, we have to filter for confirmed orders, as those are the ones which were already paid
                orders = Order.objects.filter(is_confirmed=True)
            elif val == 'Completed orders':
                all_orders = Order.objects.filter()
                # If he/she clicks on completed orders, we solely show the orders which were already shipped
                orders = []
                for order in all_orders:
                    components = OrderComponent.objects.filter(order=order)
                    # Only if all order components are complete / packaged, we can show the order to the staff member
                    if are_components_completed(components):
                        # As he/she was only looking for completed orders
                        orders.append(order)
            elif val == 'All orders':
                # All entries can easily be shown to our staff members
                orders = Order.objects.all()
            elif val == 'Orders sent':
                # We use a Boolean value is_sent to check whether our parcel has been sent yet or nor
                orders = Order.objects.filter(is_sent=True)
            elif val == 'List of products to be packed':
                # These are the products which are still missing or not yet packaged
                return show_products(request)
            elif val == 'Orders paid with cash on delivery':
                orders = Order.objects.filter(is_cash_on_delivery=True)
            elif val == 'Paid assignments to be completed':
                # The entries which are confirmed, but not yet shipped are shown in this case
                conf_orders = Order.objects.filter(is_confirmed=True)
                orders = []
                for order in conf_orders:
                    components = OrderComponent.objects.filter(order=order)
                    # To do so, we have to take all the components for that specific order
                    if are_components_completed(components) == False:
                        # and check which items are needed for that specific order
                        orders.append(order)
    else:
        # If there was no POST request yet, then we will have to show our staff member the order buttons,
        form = OrderButtons()
        # so that he/she can choose the option they would like to have displayed.
    return render(request, 'staff/manage_orders.html', {'orders': orders})


@staff_member_required
def show_order(request, id, message=None):
    # Only our staff members are allowed to view and esp. edit (all of) the orders
    order = get_object_or_404(Order, id=id)
# We take the specific order and check whether a POST request was made
    if request.method == 'POST':
        # If that was the case, we take the form to add a tracking code and check if it is valid
        form = AddDeliverySearchingCode(request.POST)
        if form.is_valid():
            # If it is valid, we set the Boolean is_sent to true
            order.is_sent = True
            # And add the tracking number to the order.
            order.delivery_searching_code = form.cleaned_data.get(
                'delivery_searching_code')
            # Moreover, the change is saved to the database
            order.save()
    else:
        # If no POST request was made yet, we show the tracking code form and take the components for that specific order
        form = AddDeliverySearchingCode()
    components = OrderComponent.objects.filter(order=order)
    # And show it to the staff member, so that he/she can add a tracking code number.
    return render(request, 'staff/show_order.html', {'order': order, 'components': components, 'form': form, 'message': message})


@staff_member_required
def refund_order(request, id):
    # Only our staff members are allowed to issue refunds.
    order = get_object_or_404(Order, id=id)
    # Initialized message is used, if the refund was already made
    message = 'The money for this order has already been refunded!'
    if not order.is_refunded:
        # If the order wasn't refunded yet, we initialize it
        payment_id = order.payment_id
        try:
            # and try to make a refund
            make_refund(payment_id)
            order.is_refunded = True
            # and save the changed is_refunded Boolean to the database
            order.save()
            # We show the amount of money which was refunded and the order number to the staff member to inform them that everything went according to plan
            message = 'The refund was successfully carried out %.2f€ for the order number %06d' % (
                order.price, order.id)
        except:
            # It could also be that a refund cannot be made / is rejected.
            message = 'A refund is not possible for this order.'
    return show_order(request, id=order.id, message=message)


@staff_member_required
def show_products(request):
    # Only our staff members are allowed to go through all order components
    components = OrderComponent.objects.all()
    # and take a look at them on the staff sites.
    return render(request, 'staff/component_list.html', {'components': components})


@staff_member_required
def completed_component(request, id):
    component = get_object_or_404(OrderComponent, id=id)
    # The only humans who are allowed to set is_complete to true are our staff members
    component.is_completed = True
    # Of course the change is saved to the database
    component.save()
    # After the product was prepared for the shipment and declared as that, the employee gets referred back to the same site he/she came from
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


@staff_member_required
def find_order(request):
    form = None
    # Only our staff members are allowed to search for orders
    orders = None
    if request.method == 'POST':
        # If the form was posted, we check whether the filter is valid
        form = FilterButton(request.POST)
        if form.is_valid():
            # If it is we take the cleaned orer id
            order_id = form.cleaned_data.get("order_id")
            # and filter with its help.
            orders = Order.objects.filter(id=order_id)
    else:
        # Otherwise the form is displayed to our staff members
        form = FilterButton()
        # So they can search for a specific order by using the order id.
    return render(request, 'staff/finder.html', {'form': form, 'orders': orders})


@staff_member_required
def read_complaints(request):
    # Only our staff members are allowed to read the complaints
    complaints = Complaint.objects.all()
    # Thus they are shown all of the complaints, as long as they aren't deleted from the database of course
    return render(request, 'staff/complaints.html', {'complaints': complaints})


@staff_member_required
def close_complaint(request, id):
    # Only our employees are allowed to close complaints
    complaint = get_object_or_404(Complaint, id=id)
    # They can do so without issuing a refund or after they have granted the refund to the customer
    complaint.is_active = False
    # The complaint is no longer active, which needs to be upated in the database
    complaint.save()
    # Redirects to the complaints.html
    return redirect('staff:complaints')


@staff_member_required
def admin_order_pdf(request, order_id):
    # Only our staff members are allowed to create a PDF from an order
    order = get_object_or_404(Order, id=order_id)
    # We take the specific order and order_details, which are the order components
    order_details = OrderComponent.objects.filter(order=order)
    # We render the relevant information to a character sequence/String
    html = render_to_string(
        'staff/pdf.html', {'order': order, 'order_details': order_details})
    # and create the response, which consists of the PDF-file
    response = HttpResponse(content_type='application/pdf')
    # The order id is used to dynamically create a filename
    response['Content-Disposition'] = 'filename="Bestellnummer_{}.pdf"'.format(
        order.id)
    # WeasyPrint takes the html string and changes it to a PDF-file.
    weasyprint.HTML(string=html).write_pdf(response)
    return response


@staff_member_required
def mark_as_paid(request, id):
    order = get_object_or_404(Order, id=id)
    order.is_confirmed = True
    order.save()
    return redirect("staff:manage_orders")


def send_pdf(order):
    # create invoice mail to send the above created PDF file
    subject = 'Solkraft - Order number {}'.format(order.id)
    # There's no need for much of a message, as it is all about that PDF-file and our emails lack any style due to the time constraints anyways
    message = 'The invoice of the last order is enclosed.'
    # The email is sent from Solkraft to the customer
    email = EmailMessage(subject, message, CONTACT_EMAIL,
                         to=[order.user.email])
    # We get all of the order components / details from the orders
    order_details = OrderComponent.objects.filter(order=order)
    # and create the string which is used to create the PDF later on
    html = render_to_string(
        'staff/pdf.html', {'order': order, 'order_details': order_details})
    # Used for writing bytes objects
    out = BytesIO()
    # We make a PDF-file out of the html string
    weasyprint.HTML(string=html).write_pdf(out)
    # attach PDF file to the email message which we have created above
    email.attach('order_{}.pdf'.format(order.id),
                 out.getvalue(), 'application/pdf')
    # and send the invoice to our customer
    email.send()

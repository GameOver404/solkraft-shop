from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from . import views

# The name is used to identify the app
# If one wants to use any function, e.g. show_order, one needs to write staff:show_order, instead of simply show_order
# One benefit of that is that one directly knows where the view function is located, which improves maintainability
app_name = 'staff'

urlpatterns = [
    # The manage orders view allows our staff to see and filter all of the orders
    path('orders/', views.manage_orders, name='manage_orders'),
    path('order/<int:id>/', views.show_order, name='show_order'),
    #     Components could be plants which are sold with planters (cachepots)
    path('completed_component/<int:id>/',
         views.completed_component, name='completed_component'),
    path('find_order/', views.find_order, name='find_order'),
    #     The PDF-file is created with WeasyPrint
    path('order/<int:order_id>/pdf/',
         views.admin_order_pdf, name='admin_order_pdf'),
    #     Refunds can be granted to customers,
    path('refund_order/<int:id>', views.refund_order, name='refund_order'),
    #     if the complaint was deemed to be acceptable
    path('mark_as_paid/<int:id>',
         views.mark_as_paid, name='mark_as_paid'),
    path('complaints', views.read_complaints, name='complaints'),
    path('close_complaint/<int:id>',
         views.close_complaint, name='close_complaint'),
]

from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe


def order_pdf(obj):
    # We have to mark it as safe, so that the attachment isn't blocked, e.g. by Django and the e-mail providers
    return mark_safe('<a href="{}">PDF</a>'.format(
        reverse('staff:admin_order_pdf', args=[obj.id])))


# The label Rechnung is shown to the administrator as an option
order_pdf.short_description = 'Invoice'

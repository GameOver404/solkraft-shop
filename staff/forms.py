
from django import forms
from cart.models import Order


class OrderButtons(forms.Form):
    # Get (more) information about a specific order
    btn = forms.CharField()


class FilterButton(forms.Form):
    # Staff can search for specific order ids
    order_id = forms.IntegerField(min_value=0)


class AddDeliverySearchingCode(forms.Form):
    # A trackin code should be added to every order, once it is sent
    delivery_searching_code = forms.CharField(max_length=200)

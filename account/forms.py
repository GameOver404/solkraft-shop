from datetime import datetime
from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password


class LoginForm(forms.Form):
    login = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)


class UserUpdateForm(forms.ModelForm):
    # TO update the user form we do not need to do much, as the User model already has the fields, our users are allowed to change
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class UserRegistrationForm(forms.ModelForm):
    # First name and last name aren't allowed to be longer than 30 characters. If someone has a ton of first names, it is fine if not all of them are wrriten down
    first_name = forms.CharField(
        label="First name", max_length=30, help_text='required')
    last_name = forms.CharField(
        label="Surname", max_length=30, help_text='required')

# The range function excludes the last number and a month has no more than 31 days.
    DAYS_CHOICES = [(i, str(i)) for i in range(1, 32)]
    # One needs to choose a month, which is slightly easier, if the name of the month is displayed. Thus, we do just that.
    MONTHS_CHOICES = (
        (1, 'January'),
        (2, 'Feburary'),
        (3, 'March'),
        (4, 'April'),
        (5, 'May'),
        (6, 'June'),
        (7, 'July'),
        (8, 'August'),
        (9, 'September'),
        (10, 'October'),
        (11, 'November'),
        (12, 'December'),
    )
    # Of course one also needs to be able to choose the year and people can only buy at our shop at a certain age. In this case 14.
    # Futhermore, none of our customers are born before 1900 and honestly that as a rather safe bet, wasn't it? :D
    YEARS_CHOICES = [(i, str(i))
                     for i in range(datetime.now().year-14, 1900, -1)]
# The user needs to choose one of each options
    day = forms.TypedChoiceField(label="Day", choices=DAYS_CHOICES)
    month = forms.TypedChoiceField(label="Month", choices=MONTHS_CHOICES)
    year = forms.TypedChoiceField(label="Year", choices=YEARS_CHOICES)
# As emails can be rather long a maximum number of 254 was chosen. Why exactly that number? Well, acutally other shops used it too. So, we figured it's a pretty good number.
    email = forms.EmailField(
        label="Email", max_length=254, help_text='required')
    # Of course we have to ensure that both passwords match and are valid
    password = forms.CharField(
        label='Password', widget=forms.PasswordInput, validators=[validate_password])
    password2 = forms.CharField(
        label='Repeat Password', widget=forms.PasswordInput, validators=[validate_password])
# The meta class if, of course, the User

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name',
                  'day', 'month', 'year', 'email')

    def clean_password2(self):
        cd = self.cleaned_data
        # If the passwords do not match, we have to tell the customer about that
        if cd['password'] != cd['password2']:
            raise forms.ValidationError(
                'The passwords do not match.')
        return cd['password2']

    def validate_birthday(self):
        day = self.cleaned_data['day']
        month = self.cleaned_data['month']
        year = self.cleaned_data['year']
        # As the user has the option to chose any day, he/she could e.g. choose the 31st Februrary
        try:
            birthday = datetime(year=int(year), month=int(month), day=int(day))
            # Of course, the 31st Februrary isn't a real date
        except ValueError:
            self.add_error('day', 'Please select a real date.')
            # and thus we return a date, which cannot be true and which is used to ensure that the user isn't created with this non-existing date
            return "01-01-0001"
            # If the user would manage to choose a date which is in the future, we do return the same "date code", which tells us that the birthday certainly isn't real
        if datetime.today() <= birthday:
            self.add_error('day', 'Date is in the future.')
            return "01-01-0001"
        return birthday

    def clean_email(self):
        email = self.cleaned_data['email']
        exists = User.objects.filter(email=email)
        # Of course, one could forget that they already have an account, but we cannot allow the same email address to be used twice
        # However, our users always have the option to reset their passwords, if needed.
        if exists:
            raise forms.ValidationError(
                "This email address is already taken. Please enter another email address.")
        return email

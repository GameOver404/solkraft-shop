from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    birthday = models.DateField(blank=True, null=True)

    @receiver(post_save, sender=User)
    # If a new user registers, he/she gets a profile
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            Profile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    # The user profile is saved to the database and can be updated by the user
    # At least the pylinter from VSCode tells us to add self to the sender (also in the function above), but that obviously wouldn't work
    def save_user_profile(sender, instance, **kwargs):
        instance.profile.save()

    def __str__(self):
        return str(self.user)

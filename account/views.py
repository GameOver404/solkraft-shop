from django.contrib.auth.models import User
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.shortcuts import render
from django.http import HttpResponse
from Solkraft import settings
from django.contrib.auth import authenticate, login, logout
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode

from shop.tokens import account_activation_token
from .forms import LoginForm, UserRegistrationForm


def user_login(request):
    #  The LoginForm, as all forms, is defined in the forms.pys
    form = LoginForm()
    # If one actually posts data
    if request.method == 'POST':
        form = LoginForm(request.POST)
        # One needs to check whether the form is acutally valid, as potentially mandatory fields were left emtpy or password and username do not match
        if form.is_valid():
            # We clean the data to solely retrieve the information the user has provided for us
            cd = form.cleaned_data
            #  We call the authenticate function to try to authenticate the user
            user = authenticate(
                request, username=cd['login'], password=cd['password'])
            # If the entered user is active (and the password match)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    # the login is carried out and thus one is logged in successfully
                    return HttpResponse('Log in successful')
                else:
                    # Otherwise the account is not acctivated yet
                    return HttpResponse('Account was deactivated')
            else:
                # Certainly one can also enter wrong information, e.g. password and username do not match
                return HttpResponse('Incorrect data entered')
        else:
            # If no POST method was carried out, we need to show the login page with the LoginForm
            form = LoginForm()
    return render(request, 'registration/login.html', {'form': form})


def register(request):
    # If one sends a filled out form
    if request.method == 'POST':
        user_form = UserRegistrationForm(request.POST)
        # we have to check whether it is filled out correctly
        if user_form.is_valid():
            birthday = user_form.validate_birthday()
            # The user has to provide a valid birthday
            if birthday != "01-01-0001":
                # If the form and birthday are valid, a new user can be saved to the database, though the account isn't active yet
                new_user = user_form.save(commit=False)
                # We need to set the password for the user
                new_user.set_password(
                    user_form.cleaned_data['password'])
                # and ensure he/she isn't active yet
                new_user.is_active = False
                #  Then we can save the user with the information to the database
                new_user.save()
                # We create a user profile for our newly created user as well
                update_profile(request, new_user, birthday)
                # Afterwards we create an email which is used to ensure that there was no attempt to commit identity fraud
                current_site = 'localhost:8000'
                mail_subject = 'Unlock Solkraft user account'
                message = render_to_string('account/acc_active_email.html',
                                           {'user': new_user,
                                            'domain': current_site,
                                            'uid': urlsafe_base64_encode(force_bytes(new_user.pk)),
                                            'token': account_activation_token.make_token(new_user),
                                            })
                to_email = user_form.cleaned_data.get('email')
                email = EmailMessage(
                    mail_subject, message, settings.CONTACT_EMAIL, to=[to_email])
                email.send()
                return render(request, 'account/confirm_mail.html', {'new_user': new_user})
    else:
        # If one hasn't sent a POST request, one needs to get the registration form displayed
        user_form = UserRegistrationForm
    return render(request, 'account/register.html', {'user_form': user_form})


def activate(request, uidb64, token):
    # Errors can (and most likely will) occur if we go live, thus we have to use a try and catch (or rather try and except) block
    try:
        # We encoe the base 64 encoded string and check whether it fits to any inactive user
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
        # If the user exists and the token fits the user, the user is set to active and the change is saved to the database
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        # One of the other 3 team members should write e-mails, which should be used for the newsletter, for example, but also here. Since the focus is not on the design part, we decided on a bare-bones implementation of the emails. Of course, in normal operation, these would have a Solkraft logo and an appealing design.

        # A second welcome message isn't sent, as the user sees that he/she is logged in and thus it would not be needed, unless we would decide to provide new users a coupon. Howver, we have a coupon for everyone, as Solkraft itself is a new shop
        # email = EmailMessage('Herzlich Willkommen bei Solkraft', 'Willkommen!', 'Solkraft@gmail.com',
        #                      to=[user.get_email()])
        # email.send()
        return render(request, 'account/register_done.html', {'new_user': user})
    else:
        # If the link isn't valid, e.g. it was already used, a template which informs the user is displayed
        return render(request, 'account/link_invalid.html')


def user_logout(request):
    # If one logs out, they are redirected to a template which informs them that their logout was successful
    logout(request)
    return HttpResponse('Logged out sucessfully')


def update_profile(request, user_id, birthday):
    # The User profile is upated
    user = user_id
    user.profile.birthday = birthday
    user.save()


def pw_reset_complete_view(request):
    # If the password reset was successful, the user sees a page which informs him/her about that
    return render(request, 'registration/password_reset_complete.html')

from django.urls import path, re_path
from django.contrib.auth import views as auth_views
from . import views

# The app_name is used to identify the application
# If one wants to use any function, e.g. login, one needs to write account:login, instead of simply login
# One benefit of that is that one directly knows where the view function is located, which improves maintainability
app_name = 'account'

urlpatterns = [
    path('register/', views.register, name='register'),
    # We overwrite the auth_views by using the same function and template names
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('login/', auth_views.LoginView.as_view(), name='login'),
    # The user receives a unique Base64 encoded string to ensure that it is truely him/her
    re_path(
        r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.activate,
        name='activate'),
    # To overwrite the password reset, the template_name needs to be defined
    path('password_reset_done/', auth_views.PasswordResetDoneView.as_view(
        template_name='registration/password_reset_done.html'), name='password_reset_done'),
    # The user receives a unique Base64 encoded string to ensure that it is truly him/her
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(success_url='reset_password_complete',
                                                                                template_name="registration/password_reset_confirm.html"),
         name='password_reset_confirm'),
    #  Once the password was changed by the user, a success page is shown
    path('reset/<token>/set-password/reset_password_complete/',
         auth_views.PasswordResetCompleteView.as_view(template_name="registration/password_reset_complete.html"),
         name='password_reset_complete'),

]

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from .models import Profile


# One reason to have the birthday in the admin panel is to check the birthdays to ensure that the customer receives a birthday coupon at the right time
# Since one's birthday usually doesn't change, the user cannot change the birthday themselves, but our staff of course can.
class Birthday(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Birthday'

# Of course the admin/staff - our staff usually also has admin rights - except for people who solely do an internship
# Thus, our staff shoul be able to view and update user information, if necessary.


class CustomUserAdmin(UserAdmin):
    inlines = (Birthday,)
    list_display = ('username', 'email', 'first_name',
                    'last_name', 'get_birthday')
    list_select_related = ('profile',)

    def get_birthday(self, instance):
        return instance.profile.birthday

    get_birthday.short_description = 'Geburtstag'

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


# Django provides a standard User model
admin.site.unregister(User)
# We however adjusted it for the needs of Solkraft
admin.site.register(User, CustomUserAdmin)

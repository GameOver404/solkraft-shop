from django.contrib import admin
from .models import Complaint


@admin.register(Complaint)
# The admin can have a look at the complaints, but our staff is taking care about the complaints manually with the help of the staff application
class ComplainAdmin(admin.ModelAdmin):
    pass

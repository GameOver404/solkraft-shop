from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from . import views

# The name is used to identify the app
# If one wants to use any function, e.g. show_profile, one needs to write profile:show_profile, instead of simply show_profile
# One benefit of that is that one directly knows where the view function is located, which improves maintainability
app_name = 'profile'

urlpatterns = [
    path('show/', views.show_profile, name='show_profile'),
    path('contact', views.make_Complaint, name='contact'),
    path('orders/', views.show_orders, name='show_orders'),
    path('update/', views.revise_profile, name='update'),
]

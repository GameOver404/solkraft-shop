from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator


class Complaint(models.Model):
    # The complaint belong to one customer
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    # And tp a specific order
    order_id = models.IntegerField(
        default=0, validators=[MinValueValidator(0)], null=True)
    # A contact email
    email = models.EmailField(max_length=254)
    # the complaint itself
    body = models.TextField(max_length=1000)
    # the date on which the complaint was made
    date = models.DateTimeField(auto_now_add=True)
    # and a Boolean field which informs the staff whether the complaint is still active and needs to be processed or whether it is finished already.
    is_active = models.BooleanField(default=True)

    class Meta:
        # The complaints are order by the date and whether or not they are active
        ordering = ['-is_active', '-date']

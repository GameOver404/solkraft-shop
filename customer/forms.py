from django import forms


class ComplainForm(forms.Form):
    # The customer needs to tell us what is wrong with his/her shipment
    body = forms.CharField(widget=forms.Textarea)
    # An email must be provided as well
    email = forms.EmailField(label='Email')
    # Just as the order.id, so that we now about which order the person is talking, as people can have multiple orders, which could contain the same item/plant
    # However, as it will be obvious in nearly all cases, it isn't a required field. It is simply a field which makes it easier for our staff.
    order_id = forms.IntegerField(
        label='Order number (without leading zeros)', min_value=0, required=False)

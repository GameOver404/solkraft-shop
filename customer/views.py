from django.shortcuts import render, get_object_or_404, redirect
from cart.models import Order, OrderComponent
from django.utils import timezone
from .forms import ComplainForm
from django.contrib.auth.models import User
from account.forms import UserUpdateForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserChangeForm
from django.http import HttpResponse
from .models import Complaint


@login_required(login_url='account:login')
# The login required as specified here will lead the customer to the login page
def show_profile(request):
    # To show the correct profile the user must be handed to the template/must be the context
    user = get_object_or_404(User, id=request.user.id)
    return render(request, 'customer/profile.html', {'user': user})


@login_required(login_url='account:login')
# The login required as specified here will lead the customer to the login page
def revise_profile(request):
    # If one posts the form
    if request.method == 'POST':
        # We will take the information from the user profile, if it exists, otherwise we will leave it empty
        # and we will add the changes from the customer into the form - be it new or updated information
        form = UserUpdateForm(request.POST or None, instance=request.user)
        # If the form is valid, we save the information to the database and show the updated profile
        # This also works if no changes were made to the database
        if form.is_valid():
            form.save()
            return redirect('profile:show_profile')
    # If one loads the page and thus there is no POST yet, one gets the UserUpateForm displayed
    else:
        form = UserUpdateForm(instance=request.user)
        return render(request, 'customer/profile_update.html', {'form': form})


@login_required(login_url='account:login')
# The login required as specified here will lead the customer to the login page
def show_orders(request):
    # To show the order we need to get the right user
    user = get_object_or_404(User, id=request.user.id)
    # as well as the right order object(s)
    orders = Order.objects.filter(user=user)
# Every order is shown
    for order in orders:
        # We delete all the deprecated orders
        if (order.is_confirmed == False and order.expiration_date < timezone.now()):
            order.delete()
            # We order the orders in such a way, that the latest order is displayed first
    orders = Order.objects.filter(user=user).order_by('-created_at')
    order_components = []
    # Then we go through all orders and the components of the order to show them to the customer
    for order in orders:
        # We only take the orders from the customer
        components = OrderComponent.objects.filter(order=order)
        # We aslo show all of the products within the order
        for component in components:
            # and thus create the order_component list
            order_components.append(component)
    # Afterwards all the relevant contect is given to the orders.html
    return render(request, 'customer/orders.html', {'orders': orders, 'components': order_components})


def make_Complaint(request):
    # Only logged in users are allowed to make requests, but the form itself can be viewed by anyone whether they are logged in or not
    user = None
    form = ComplainForm()
    message = None
    # If they are logged in, we can identify the user
    if request.user.is_authenticated:
        user = request.user
        # Otherwise we will have to display a message to tell our user to login in order to make a complaint
    else:
        message = 'Only logged in users can send complaints.'
        return render(request, 'customer/complaint.html', {'form': form, 'message': message})
    # If the user filled out the form and posted it
    if request.method == "POST":
        form = ComplainForm(request.POST)
        # We check whether the form is vali and take the important data
        if form.is_valid():
            form = form.cleaned_data
            body = form['body']
            email = form['email']
            order_id = form['order_id']
            # to create a new complaint
            comp = Complaint.objects.create(
                user=user, body=body, email=email, order_id=order_id)
            # And display a message that their message was sent successfully
            message = 'Message sent. You will receive a reply to your email address: %s' % email
            # Please note that no initial email is sent to the customer, but after our staff has checked the complaint the user receives an email from them,
            # as well as an email from PayPal, if they have paid with PayPal and the refund was granted via PayPal
            return render(request, 'customer/complaint.html', {'form': form, 'message': message})
    return render(request, 'customer/complaint.html', {'form': form, 'message': message})

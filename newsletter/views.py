from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Subscriber
from Solkraft.settings import CONTACT_EMAIL
from django.core.mail import EmailMessage
from .forms import SubscriberForm
import random


@csrf_exempt
# Suppresses the cross-site request forgery check, as it is not needed in this case
def new(request):
    form = SubscriberForm()
    # When submitting the subscriber form and the form is valid
    if request.method == 'POST':
        if form.is_valid:
            # the backend checks if the email is already registered
            subscrib = Subscriber.objects.all()
            for sub in subscrib:
                if sub.email == request.POST['email']:
                    # if the email is already registered, we need to inform the user about it
                    return render(request, 'newsletter/newsletter_registration.html', {'email': request.POST['email'], 'action': 'already found in subscriber list', 'form': SubscriberForm()})
            # Generate random number to make sure the email really belongs to the subscriber (not super sure, but it's easy for a newsletter of)
            sub = subscriber(
                email=request.POST['email'], conf_num=random_digits())
            sub.save()
            # send confirmation email
            html_content = 'Thank you for subscribing to the Solkraft newsletter! \n{}Please complete the registration by clicking on the following link: \n{}/confirm/?email={}&conf_num={}'.format(request.build_absolute_uri(''),
                                                                                                                                                                                                     sub.email,
                                                                                                                                                                                                     sub.conf_num)
            email = EmailMessage(
                'Your Solkraft newsletter subscription', html_content, 'Solkraft.GreenEnergySolutions@gmail.com', to=[sub.email])
            email.send()
            # We also add a message to let the user know that the subscription worked
            return render(request, 'newsletter/newsletter_registration.html', {'email': sub.email, 'action': 'added'})
        else:
            pass
    else:
        pass


def random_digits():
    # Since this is a simple newsletter, we didn't use base64, but randint, which is only partially random, but works well enough in this case
    return "%0.12d" % random.randint(0, 9999999999)


def newsletter_registration(request):
    # if you want to register, you have to fill in the subscriber form
    form = SubscriberForm()
    return render(request, 'newsletter/newsletter_registration.html', {'form': SubscriberForm()})


def confirm(request):
    # the subscriber must click on the link provided in the email to activate his subscription
    sub = Subscriber.objects.get(email=request.GET['email'])
    # The backend checks if the conf_num specified by the user/link matches the one in the database
    if sub.conf_num == request.GET['conf_num']:
        # if it does, the subscriber is confirmed and the change is saved in the database
        sub.confirmed = True
        sub.save()
        # The information about the successful operation is also displayed to the user
        return render(request, 'newsletter/newsletter_registration.html', {'email': sub.email, 'action': 'confirmed'})
    else:
        # if something goes wrong, the user will of course be informed as well
        return render(request, 'newsletter/newsletter_registration.html', {'email': sub.email, 'action': 'rejected'})


def delete(request):
    # In each newsletter, our subscribers will find the option to unsubscribe.
    sub = Subscriber.objects.get(email=request.GET['email'])
    # A conf_num is always given and this is how Solkraft checks if it is the right number
    if sub.conf_num == request.GET['conf_num']:
        # and if it is, it deletes the user from the database
        sub.delete()
        # and tells him/her that
        return render(request, 'newsletter/newsletter_registration.html', {'email': sub.email, 'action': 'unsubscribed'})
    else:
        # if something goes wrong, the user will also be informed.
        return render(request, 'newsletter/newsletter_registration.html', {'email': sub.email, 'action': 'unsubscribed'})

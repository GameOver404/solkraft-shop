from django.db import models
from Solkraft import settings
from django.core.mail import EmailMessage


class Subscriber(models.Model):
    # The Subscriber model also contains a conf_num which ensures that nobody receives our newsletter "by accident"
    email = models.EmailField(unique=True)
    conf_num = models.CharField(max_length=15)
    # Our newsletter is therefore solely sent to confirmed Subscribers and initially they are not confirmed
    confirmed = models.BooleanField(default=False)

    def __str__(self):
        return self.email + " (" + ("not " if not self.confirmed else "") + "confirmed)"


class Newsletter(models.Model):
    # We save the information on when the newsletter was created and updated
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # As well as the subject
    subject = models.CharField(max_length=150)
    # and the content should be uploaded as HTML (though txt works fine as well) in the admin portal
    contents = models.FileField(upload_to='uploaded_newsletters/')

    def __str__(self):
        # Adjust display of created at time
        return self.subject + " " + self.created_at.strftime("%B %d, %Y")

    def send(self, request):
        contents = self.contents.read().decode('utf-8')
        # As mentioned above solely confirmed users receive our emails
        subscribers = Subscriber.objects.filter(confirmed=True)
        # Every single one of them receives the newsletter
        for sub in subscribers:
            # which also contains an option to unsubscribe
            html_content = contents + (
                '\nVom Newsletter abmelden:\n{}/delete/?email={}&conf_num={}').format(request.build_absolute_uri('/newsletter'),
                                                                                      sub.email,
                                                                                      sub.conf_num)
            email = EmailMessage(
                self.subject,
                html_content,
                settings.CONTACT_EMAIL,
                [sub.email],)
            # The mail is sent via SendGrid
            email.send()

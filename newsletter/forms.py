from django import forms


class SubscriberForm(forms.Form):
    # The subscriber form has only one field, which is the email address
    email = forms.EmailField(label='Your email address',
                             max_length=225,
                             widget=forms.EmailInput(attrs={'class': 'form-control'}))

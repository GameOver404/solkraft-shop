from django.contrib import admin
from .models import Subscriber, Newsletter

# Our admins have a option to send newsletters to the subscribers via the admin panel


def send_newsletter(modeladmin, request, queryset):
    for newsletter in queryset:
        newsletter.send(request)


# The text in quotes is displayed as information for our admins
send_newsletter.short_description = "Send selected newsletter to all subscribers"

# Except for sending the newsletter


class NewsletterAdmin(admin.ModelAdmin):
    actions = [send_newsletter]


# Our admins can also add or delete subscribers and newsletters
admin.site.register(Subscriber)
admin.site.register(Newsletter, NewsletterAdmin)

from . import views
from django.urls import path

# The name is used to identify the app
# If one wants to use any function, e.g. new, one needs to write newsletter:new, instead of simply new
# One benefit of that is that one directly knows where the view function is located, which improves maintainability
app_name = 'newsletter'

urlpatterns = [
    path('new/', views.new, name='new'),
    path('new//confirm/', views.confirm, name='confirm'),
    path('delete/', views.delete, name='delete'),
    path('newsletter_registration/', views.newsletter_registration,
         name="newsletter_registration"),
]

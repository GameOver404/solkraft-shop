from django.contrib import admin

from .models import BlogEntry


# The admin can write, update and delete blog entries
admin.site.register(BlogEntry)

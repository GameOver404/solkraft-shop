from django.shortcuts import render, get_object_or_404
from . import models


def allblogs(request):
    blogentries = models.BlogEntry.objects
    # As context, we return all the objects within BlogEntry, as we want to display every blog entry.
    return render(request, 'allblogs.html', {"blogs": blogentries})


def blog_post(request, post_id):
    # To display a blog post, we have to retrive it and if we cannot find it, we have to return a 404 message
    single_post = get_object_or_404(models.BlogEntry, pk=post_id)
    # Of course, the single post is the context in this case
    return render(request, 'blogpost.html', {'single_blog': single_post})


def games(request):
    # The games site is a static page
    return render(request, 'games.html')

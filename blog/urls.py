from django.urls import path
from . import views

# The name is used to identify the app
# If one wants to use any function, e.g. allblogs, one needs to write blog:allblogs, instead of simply allblogs
# One benefit of that is that one directly knows where the view function is located, which improves maintainability
app_name = 'blog'

urlpatterns = [
    path('', views.allblogs, name='allblogs'),
    path('games.html', views.games, name='games'),
    # The blog post is identified by it's id, which is an intger
    path('<int:post_id>', views.blog_post, name='blog_post')
]

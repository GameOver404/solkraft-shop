from django.db import models
from django.utils import timezone


class BlogEntry(models.Model):
    title = models.CharField(max_length=100)
    pub_date = models.DateTimeField(verbose_name='Publication Date')
    # The image is uploaded to the media folder and there it is saved inside the images folder
    image = models.ImageField(upload_to='images/')
    body = models.TextField()

    class Meta:
        # We order the blog entries by the publication date. The - is needed to have the latest blog entry first
        ordering = ['-pub_date', ]
        # We've added a verbose name to display it more nicely in the admin panel
        verbose_name = 'blog entry'
        # and BlogEntrys would be rather bad English anyways and we wouldn't want that
        verbose_name_plural = 'blog entries'

    def __str__(self):
        return self.title

    def pub_date_pretty(self):
        # We improve the look of the publication date, before returning it
        self.pub_date = self.pub_date.strftime("%d %b %Y")
        return self.pub_date

    def summary(self):
        # If the text of the blog entry is smaller or equal to 100 words, we can display it entirely
        if len(self.body.split()) <= 100:
            return self.body
        else:
            # Otherwise, we can only show the first 100 words
            words = self.body.split()
            word_list = []
            i = 0
            while i < 100:
                word_list += words[i]+" "
                i += 1
                # and add three dots to indicate that this isn't the entire text of the post.
            words = "".join(word_list)+" ..."
            return words
